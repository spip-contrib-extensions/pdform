<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin PDForm
 *
 * @plugin     Pdform
 * @copyright  2021
 * @author     Thrax
 * @licence    GNU/GPL
 * @package    SPIP\Pdform\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Fonction d'installation et de mise à jour du plugin.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function pdform_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	$maj['create'] = array(      
		array('maj_tables', array('spip_pdform_patrons','spip_pdform_cellules_liens','spip_pdform_cellules','spip_pdform_images_liens','spip_pdform_images','spip_pdform_patrons_liens')), 
		array('ecrire_config','pdform/tables_nourricieres', array('spip_auteurs')),
		array('ecrire_config','pdform/formulaire_sur_patron', 'on'),
		array('ecrire_config','pdform/peuplement', 'on'),
		array('pdform_create'),
  );
 
	$maj['1.0.1'] = array(
		# Activer le formulaire par défaut
		array('ecrire_config','pdform/formulaire_sur_patron', 'on'),
	);
	$maj['1.0.2'] = array(
		# Activer le formulaire par défaut
		array('ecrire_config','pdform/peuplement', 'on'),
	);
	
	$maj['1.0.3'] = array(
		# permettre les associations de patrons sur des objets
		array('maj_tables', array(
			'spip_pdform_patrons_liens')
		)
	);
	$maj['1.0.4'] = array(
		# pouvoir donner un rôle lors de l'association des patrons
		array('sql_alter', 'TABLE spip_pdform_patrons_liens ADD COLUMN role varchar(25) DEFAULT "" NOT NULL after objet'),
		// faire tomber la PRIMARY KEY et la remettre
		array('sql_alter', 'TABLE spip_pdform_patrons_liens DROP PRIMARY KEY'),
		array('sql_alter', 'TABLE spip_pdform_patrons_liens ADD PRIMARY KEY(id_pdform_patron, id_objet, objet, role)'),
	);
	
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);

}

/**
 * Fonction chargée à la création: création de sous repertoires de stockage des pdfs
 *
 * @return void
**/

function pdform_create() {
	# un répertoire permanent, IMG/pdform
	include_spip('inc/documents');
	creer_repertoire_documents('pdform');
	# un répertoire temporaire, que l'on peut purger avec le cache local/cache-pdform
	if (sous_repertoire( _DIR_VAR . "cache-pdform/" )){
		spip_log( "impossible de creer le repertoire temporaire ’" . _DIR_VAR . "cache-pdform/’" , "pdform" );
	}
}


/**
 * Fonction de désinstallation du plugin.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function pdform_vider_tables($nom_meta_base_version) {
	# suppression des tables
	sql_drop_table('spip_pdform_patrons');
	sql_drop_table('spip_pdform_patrons_liens');
	sql_drop_table('spip_pdform_cellules');
	sql_drop_table('spip_pdform_cellules_liens');
	sql_drop_table('spip_pdform_images');
	sql_drop_table('spip_pdform_images_liens');
	
	effacer_config("pdform/tables_nourricieres");
	effacer_meta($nom_meta_base_version);
}

?>