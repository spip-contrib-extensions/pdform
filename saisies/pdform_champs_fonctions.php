<?php
 /**
  * Fonctions spécifiques au sequelette de la saisie
  *
  * @plugin     Pdform
  * @copyright  2021
  * @author     Vincent CALLIES
  * @licence    GNU/GPL
  * @package    SPIP\Pdform\Saisies
  */
 
 if (!defined('_ECRIRE_INC_VERSION')) return;
 
 /**
 * Lister l'ensemble des champs qui seront affichés sur le formulaire PDF
 * 
 * @param string $table_objet
 *     nom de la table de l'objet dont on veut retourner les champs éditables ou plus
 * todo @param int $id_pdform_patron
 *     identifiant unique du patron concerné par les champs
 * @return array|void
 */
function pdform_lister_champs ($table_objet = '') {

	# pas de table, pas de champs éditables
	if (!strlen($table_objet)) return;

	include_spip('inc/filtres'); // pour objet_info
	# on va cherche le nom de l'objet...
	if ($objet = table_objet($table_objet)){
		# ...puis les champs éditables de l'objet
		$champs = objet_info($objet, 'champs_editables');
	}
	# on ajoute la clé de l'id unique
	
	# on ajoute la possibilité d'un bloc (fusion de champs éditables au sein d'une seule cellule)
	
	# Envoyer aux plugins pour qu'ils complètent (ou altèrent) la liste
 	$champs = pipeline('champs_pdform', $champs);
	
	return $champs;
	
	}