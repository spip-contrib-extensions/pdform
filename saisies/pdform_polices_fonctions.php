<?php
 /**
  * Fonctions spécifiques au sequelette de la saisie
  *
  * @plugin     Pdform
  * @copyright  2021
  * @author     Vincent CALLIES
  * @licence    GNU/GPL
  * @package    SPIP\Pdform\Saisies
  */
 
 if (!defined('_ECRIRE_INC_VERSION')) return;
 
 /**
 * Lister l'ensemble des champs qui seront affichés sur le formulaire PDF
 * 
 * @param string $table_objet
 *     nom de la table de l'objet dont on veut retourner les champs éditables ou plus
 * todo @param int $id_pdform_patron
 *     identifiant unique du patron concerné par les champs
 * @return array|void
 */
function pdform_lister_polices () {

	$retour = false;
	# pas de constante, pas de champs éditables
	if (!defined('FPDF_FONTPATH')) return; // définition de la constante pour les polices utilisable par le PDF

	# on va chercher les polices...

	/* Les polices sont décrites par des fichiers php
	 * placés dans le répertoire FPDF_FONTPATH
	 * et sous un nom POLICE.php suivi de déclinaisons éventuelles par styles
	 * POLICEb.php, POLICEbi.php et POLICEi.php
	 * un regex est donc définie pour exclure ces derniers fichiers
	 */

	$regex = "[^/]+[^b|bi|i]\.php$";
	$fichiers = find_all_in_path(FPDF_FONTPATH, $regex);
	
	/* La fonction find_all_in_path retourne un tableau
	 * nom du fichier -> chemin
	 * qu'il faut remettre en forme pour un usage dans la saisie
	 * nom de la police -> nom de la police
	 */
	if ($fichiers and is_array($fichiers))
	{
		$retour = array();
		foreach ($fichiers as $cle => $valeur)
		{
			$nom = substr($cle, 0, -4);
			$retour[$nom] = ucfirst($nom);
		}
	}
	return $retour;
	
	}