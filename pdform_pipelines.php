<?php
/**
 * Utilisations de pipelines par Pdform
 *
 * @plugin     Pdform
 * @copyright  2021
 * @author     Thrax
 * @licence    GNU/GPL
 * @package    SPIP\Pdform\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Ajouter les objets sur les vues des parents directs
 *
 * @pipeline affiche_enfants
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
**/
function pdform_affiche_enfants($flux) {
	if ($e = trouver_objet_exec($flux['args']['exec']) and $e['edition'] == false) {
		$id_objet = $flux['args']['id_objet'];
	}
	return $flux;
}

/**
 * Utiliser ce pipeline permet d'ajouter du contenu sur certaines pages,
 * notamment des formulaires de liaisons entre objets
 *
 * @pipeline affiche_milieu
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function pdform_affiche_milieu($flux) {
	$texte = '';

	if (isset($flux['args']['exec']) and $e = trouver_objet_exec($flux['args']['exec']) 
		and isset($e['type']) and $type = $e['type'] 
		and isset($e['id_table_objet'])  and isset($flux['args'][$e['id_table_objet']]) and $id = intval($flux['args'][$e['id_table_objet']])
	){

		if (!$e['edition'] and $type == 'pdform_patron') {
	
			# auteurs sur pdform_patron
			$texte .= recuperer_fond(
				'prive/objets/editer/liens',
				array(
					'table_source'=>'auteurs',
					'objet' => $type,
					'id_objet' => $id,
				#'editable'=>autoriser('associerauteurs',$e['type'],$e['id_objet'])?'oui':'non'
				)
			);
	
			# pdform_cellule sur pdform_patron
			$texte .= recuperer_fond(
				'prive/objets/editer/liens',
				array(
					'table_source'=>'pdform_cellules',
					'objet' => $type,
					'id_objet' => $id,
				#'editable'=>autoriser('associerpdformcellules',$e['type'],$e['id_objet'])?'oui':'non'
				)
			);
			
			# pdform_image sur pdform_patron
			$texte .= recuperer_fond(
				'prive/objets/editer/liens',
				array(
					'table_source'=>'pdform_images',
					'objet' => $type,
					'id_objet' => $id,
				#'editable'=>autoriser('associerpdformcellules',$e['type'],$e['id_objet'])?'oui':'non'
				)
			);
		}

		if ($texte) {
			if ($p=strpos($flux['data'],"<!--affiche_milieu-->"))
				$flux['data'] = substr_replace($flux['data'],$texte,$p,0);
			else
				$flux['data'] .= $texte;
		}
	}

	return $flux;
}

/**
 * Utiliser ce pipeline permet d'ajouter des information sur la colonne gauche,
 *
 * @pipeline affiche_gauche
 * @use find_all_in_path qui retourne un tableau ayant pour clé le nom du fichier et pour valeur le nom du fichier précédé par son chemin
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function pdform_affiche_gauche($flux) {
	$exec = $flux['args']['exec'];

 /**
  * Un squelette dans le prive/squelettes/navigation/pdform_patrons.html serait possible (voir les premières versions du plugin)
  * mais sa complexité pose des problèmes avec le paramétrage de certains hébergeurs
  */

	if ($exec == 'pdform_patrons' // au sein de la liste des patrons, avec la colonne gauche...
		and lire_config('pdform/peuplement') // ... on peut peupler
		and include_spip('inc/autoriser')
		and autoriser('peuplement',$exec) // ... on a le droit de peupler
		and include_spip('inc/presentation')
	){
		# visualiser les peuplements possibles
		$dir = 'action/';
		$pattern = 'pdform_peuplement';
		$liste_de_fichiers = find_all_in_path($dir, $pattern);
		$texte = '<ul class="spip liste">';
		$securiser_action = charger_fonction('securiser_action', 'inc');
		foreach ($liste_de_fichiers as $nom_du_fichier => $chemin_et_nom) {
			$texte .= '<li><a href="'
				. $securiser_action(
					substr($nom_du_fichier, 0, -4),
					'',
					generer_url_ecrire($exec)
				)
				. '" title="' . _T('pdform_peuplement:title_'.substr($nom_du_fichier, 0, -4)) . '">'
				. _T('pdform_peuplement:titre_'.substr($nom_du_fichier, 0, -4)) . "</a></li>";
		}
		$texte .= '</ul>'; 
		// proposer une aide sur le peuplement : Générer un lien d'aide (icône + lien)
		$aide = charger_fonction('aide', 'inc');
		$aider = $aide('pdformpeuplements');
// Corriger : debut_cadre_relief est OBSOLETE ! 
// Il faut utiliser boite_ouvrir à la place
		$flux['data'] .= debut_cadre_relief('base-24.svg',true,'', _T('pdform_peuplement:titre_peuplement') . $aider)
			. $texte // recuperer_fond('prive/squelettes/inclure/equipe_uais_section')
			. fin_cadre_relief(true);
	}

	return $flux;
}

/**
 * Utiliser ce pipeline permet d'ajouter une aide au plugin,
 *
 * @pipeline aide_index
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function pdform_aide_index($index) {
	$index['pdformcellules'] = [
		'liaisons'
	];
	$index['pdformimages'] = [
		'liaisons'
	];
	$index['pdformpatrons'] = [
		'statuts',
	];
	$index['pdformpages'] = [
		'formats',
		'orientations',
		'pages',
	];
	$index['pdformfontes'] = [
		'polices',
		'tailles',
		'styles',
	];
	$index['pdformpeuplements'] = [
		'peuplements',
		'plugin',
	];
	return $index;
}