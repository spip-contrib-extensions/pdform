<?php
/**
 * Déclarations relatives à la base de données
 *
 * @plugin     Pdform
 * @copyright  2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Pdform\Base
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function pdform_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['pdform_patrons'] = 'pdform_patrons';
	$interfaces['table_des_tables']['pdform_cellules'] = 'pdform_cellules';
	$interfaces['table_des_tables']['pdform_images'] = 'pdform_images';
	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function pdform_declarer_tables_objets_sql($tables) {

	# En couture, un patron est la représentation d'un vêtement. Ici, c'est la préparation du PDF.

	$tables['spip_pdform_patrons'] = [
		'type' => 'pdform_patron',
		'principale' => 'oui',
		'table_objet_surnoms' => ['pdformpatron'], 
		'page' => '',
		'field' => [
			'id_pdform_patron' => 'bigint(21) NOT NULL',
			'titre' => "text DEFAULT '' NOT NULL",
			'orientation' => "ENUM('P', 'L') DEFAULT 'P' ",
			'format' => "ENUM('A10', 'A09', 'A8', 'A7', 'A6', 'A5', 'A4', 'A3', 'A2', 'A1', 'A0') DEFAULT 'A4' ",
			'pages' => "int(4) DEFAULT '0' NOT NULL",
			'police' => "text DEFAULT '' NOT NULL",
			'style' => "ENUM('', 'B', 'I', 'U') DEFAULT '' ",
			'taille' => "int(4) DEFAULT '0' NOT NULL",
			'couleur_trace' => "text DEFAULT '' NOT NULL", // SetDrawColor
			'couleur_rempli' => "text DEFAULT '' NOT NULL", // SetFillColor
			'couleur_texte' => "text DEFAULT '' NOT NULL", // SetTextColor

			'entete_police' => "text DEFAULT '' NOT NULL", // SetFont
			'entete_style' => "ENUM('', 'B', 'I', 'U') DEFAULT '' ", 
			'entete_taille' => "text DEFAULT '' NOT NULL",
			'entete_couleur_trace' => "text DEFAULT '' NOT NULL", // SetDrawColor
			'entete_couleur_rempli' => "text DEFAULT '' NOT NULL", // SetFillColor
			'entete_couleur_texte' => "text DEFAULT '' NOT NULL", // SetTextColor
			'entete_x' => "DECIMAL(5,2) DEFAULT '0.00' NOT NULL", // SetXY
			'entete_y' => "DECIMAL(5,2) DEFAULT '0.00' NOT NULL", 
			'entete_largeur' => "DECIMAL(5,2) DEFAULT '0.00' NOT NULL",  // Cell
			'entete_hauteur' => "DECIMAL(5,2) DEFAULT '0.00' NOT NULL", 
			'entete_texte' => "text DEFAULT '' NOT NULL", 
			'entete_bordure' =>  "ENUM('0', '1', 'L', 'T', 'R', 'B') DEFAULT '0' ",
			'entete_apres' => "ENUM('0', '1', '2') DEFAULT '0' ",
			'entete_align' =>  "ENUM('L', 'C', 'R') DEFAULT 'L' ",
			'entete_plein' => "ENUM('0', '1') DEFAULT '0' ",
			'entete_lien' => "text DEFAULT '' NOT NULL",
			'entete_pages' => "text DEFAULT '' NOT NULL",

			'pied_police' => "text DEFAULT '' NOT NULL", // SetFont
			'pied_style' => "ENUM('', 'B', 'I', 'U') DEFAULT '' ", 
			'pied_taille' => "text DEFAULT '' NOT NULL",
			'pied_couleur_trace' => "text DEFAULT '' NOT NULL", // SetDrawColor
			'pied_couleur_rempli' => "text DEFAULT '' NOT NULL", // SetFillColor
			'pied_couleur_texte' => "text DEFAULT '' NOT NULL", // SetTextColor
			'pied_x' => "DECIMAL(5,2) DEFAULT '0.00' NOT NULL", // SetXY
			'pied_y' => "DECIMAL(5,2) DEFAULT '0.00' NOT NULL", 
			'pied_largeur' => "DECIMAL(5,2) DEFAULT '0.00' NOT NULL",  // Cell
			'pied_hauteur' => "DECIMAL(5,2) DEFAULT '0.00' NOT NULL", 
			'pied_texte' => "text DEFAULT '' NOT NULL", 
			'pied_bordure' =>  "ENUM('0', '1', 'L', 'T', 'R', 'B') DEFAULT '0' ",
			'pied_apres' => "ENUM('0', '1', '2') DEFAULT '0' ",
			'pied_align' =>  "ENUM('L', 'C', 'R') DEFAULT 'L' ",
			'pied_plein' => "ENUM('0', '1') DEFAULT '0' ",
			'pied_lien' => "text DEFAULT '' NOT NULL",
			'pied_pages' => "text DEFAULT '' NOT NULL",

			'date' => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			'statut' => 'varchar(20) DEFAULT "0" NOT NULL',
			'maj' => 'TIMESTAMP',
		],
		'key' => [
			'PRIMARY KEY' => 'id_pdform_patron',
			'KEY statut' => "statut",
		],
		'titre' => "titre AS titre, '' AS lang",
		'date' => 'date',
		'champs_editables' => [
			'titre', 
			'pages', 
			'orientation', 
			'format', 
			'police', 
			'style', 
			'taille', 
			'couleur_trace', 
			'couleur_rempli', 
			'couleur_texte',
			'entete_police',
			'entete_style', 
			'entete_taille', 
			'entete_couleur_trace', 
			'entete_couleur_rempli', 
			'entete_couleur_texte', 
			'entete_x', 
			'entete_y', 
			'entete_largeur', 
			'entete_hauteur', 
			'entete_texte', 
			'entete_bordure', 
			'entete_apres', 
			'entete_align', 
			'entete_plein', 
			'entete_lien', 
			'entete_pages',
			'pied_police',
			'pied_style', 
			'pied_taille', 
			'pied_couleur_trace', 
			'pied_couleur_rempli', 
			'pied_couleur_texte', 
			'pied_x', 
			'pied_y', 
			'pied_largeur', 
			'pied_hauteur', 
			'pied_texte', 
			'pied_bordure', 
			'pied_apres', 
			'pied_align', 
			'pied_plein', 
			'pied_lien', 
			'pied_pages'
		],
		'champs_versionnes' => [
			'titre', 
			'pages', 
			'orientation', 
			'format', 
			'police', 
			'taille', 
			'couleur_trace', 
			'couleur_rempli', 
			'couleur_texte',
			'entete_police',
			'entete_style', 
			'entete_taille', 
			'entete_couleur_trace', 
			'entete_couleur_rempli', 
			'entete_couleur_texte', 
			'entete_x', 
			'entete_y', 
			'entete_largeur', 
			'entete_hauteur', 
			'entete_texte', 
			'entete_bordure', 
			'entete_apres', 
			'entete_align', 
			'entete_plein', 
			'entete_lien',
			'pied_police',
			'pied_style', 
			'pied_taille', 
			'pied_couleur_trace', 
			'pied_couleur_rempli', 
			'pied_couleur_texte', 
			'pied_x', 
			'pied_y', 
			'pied_largeur', 
			'pied_hauteur', 
			'pied_texte', 
			'pied_bordure', 
			'pied_apres', 
			'pied_align', 
			'pied_plein', 
			'pied_lien'
		],
		'rechercher_champs' => [
			'titre' => 8
		],
		'rechercher_jointures' => [],
		'roles_colonne' => 'role',
		'join' => [
			'id_pdform_patron' => 'id_pdform_patron'
		],
		'tables_jointures' => [],
		'statut' => [
			[
				'champ' => 'statut',
				'publie' => 'publie',
				'previsu' => 'publie,prop,prepa/auteur',
				'post_date' => 'date',
				'exception' => array('statut', 'tout')
			]
		],
		'statut_titres' => [
			'prepa' => 'pdform_patron:info_statut_redaction',
			'prop' => 'pdform_patron:info_statut_propose',
			'publie' => 'pdform_patron:info_statut_publie',
			'refuse' => 'pdform_patron:info_statut_refuse',
			'poubelle' => 'pdform_patron:info_statut_supprime'
		],
		'statut_textes_instituer' => [
			'prepa' => 'texte_statut_en_cours_redaction',
			'prop' => 'texte_statut_propose_evaluation',
			'publie' => 'texte_statut_publie',
			'refuse' => 'texte_statut_refuse',
			'poubelle' => 'texte_statut_poubelle',
		],
		'texte_changer_statut' => 'pdform_patron:texte_changer_statut',
		//'aide_changer_statut' =>'pdformpatrons/statuts',
	];

	/**
	 * La définition de la table accueillant les cellules.
	 * Une cellule représente la définition de l'affichage d'un champ éditable.
	 * Une cellule peut être associée au patron,
	 * ce qui donne au patron la possibilité d'afficher les champs.
	 */

	$tables['spip_pdform_cellules'] = [
		'type' => 'pdform_cellule',
		'principale' => "oui",
		'table_objet_surnoms' => ['pdformcellule'], 
		'page' => '',
		'field' => [
			'id_pdform_cellule' => 'bigint(21) NOT NULL',
			# 'id_pdform' -> par liaison, pour une mutualisation de la définition
			'titre' => "text DEFAULT '' NOT NULL",
			'table_nourriciere' => "VARCHAR(255) DEFAULT '' NOT NULL",
			'police' => "text DEFAULT '' NOT NULL", // SetFont
			'style' => "ENUM('', 'B', 'I', 'U') DEFAULT '' ", 
			'taille' => "text DEFAULT '' NOT NULL",
			'couleur_trace' => "text DEFAULT '' NOT NULL", // SetDrawColor
			'couleur_rempli' => "text DEFAULT '' NOT NULL", // SetFillColor
			'couleur_texte' => "text DEFAULT '' NOT NULL", // SetTextColor
			'x' => "DECIMAL(5,2) DEFAULT '0.00' NOT NULL", // SetXY
			'y' => "DECIMAL(5,2) DEFAULT '0.00' NOT NULL", 
			'largeur' => "DECIMAL(5,2) DEFAULT '0.00' NOT NULL",  // Cell
			'hauteur' => "DECIMAL(5,2) DEFAULT '0.00' NOT NULL", 
			'texte' => "text DEFAULT '' NOT NULL", 
			'bordure' =>  "ENUM('0', '1', 'L', 'T', 'R', 'B') DEFAULT '0' ",
			'apres' => "ENUM('0', '1', '2') DEFAULT '0' ",
			'align' =>  "ENUM('L', 'C', 'R') DEFAULT 'L' ",
			'plein' => "ENUM('0', '1') DEFAULT '0' ",
			'lien' => "text DEFAULT '' NOT NULL",

			'maj' => 'TIMESTAMP'
		],
		'key' => [
			'PRIMARY KEY' => 'id_pdform_cellule',
		],
		'titre' => "titre AS titre, '' AS lang",
		#'date' => "",
		'champs_editables' => [
			'titre', 
			'table_nourriciere',
			'police',
			'style', 
			'taille', 
			'couleur_trace', 
			'couleur_rempli', 
			'couleur_texte', 
			'x', 
			'y', 
			'largeur', 
			'hauteur', 
			'texte', 
			'bordure', 
			'apres', 
			'align', 
			'plein', 
			'lien'
		],
		'champs_versionnes' => [
			'titre', 
			'table_nourriciere', 
			'setx', 
			'sety', 
			'police', 
			'settextcolor', 
			'fontstyle', 
			'cellw', 
			'cellh', 
			'celltxt', 
			'cellborder', 
			'cellln', 
			'cellalign', 
			'cellfill', 
			'celllink'
		],
		'rechercher_champs' => [
			'titre' => 8
		],
		'rechercher_jointures' => [],
		'tables_jointures' => [
			'pdform_cellules_liens'
		],
	];
	

	/**
	 * La définition de la table accueillant les images à inclure.
	 * Une images peut être associée au patron,
	 * ce qui donne au patron la possibilité de l'appeler.
	 */

	$tables['spip_pdform_images'] = [
		'type' => 'pdform_image',
		'principale' => "oui",
		'page' => '',
		'table_objet_surnoms' => [
			'pdformimage'
		], 
		'field' => [
			'id_pdform_image' => 'bigint(21) NOT NULL',
			'titre' => "text DEFAULT '' NOT NULL",
			'imgfile' => "text DEFAULT '' NOT NULL",
			'imgx' => "DECIMAL(5,2) DEFAULT '0.00' NOT NULL",
			'imgy' => "DECIMAL(5,2) DEFAULT '0.00' NOT NULL",
			'imgw' => "DECIMAL(5,2) DEFAULT '0.00' NOT NULL",
			'imgh' => "DECIMAL(5,2) DEFAULT '0.00' NOT NULL",
			'imgtype' => "text DEFAULT '' NOT NULL",
			'imglink' => "text DEFAULT '' NOT NULL",
			'maj' => 'TIMESTAMP'
		],
		'key' => [
			'PRIMARY KEY' => 'id_pdform_image',
		],
		'titre' => "titre AS titre, '' AS lang",
		'champs_editables' => [
			'titre', 
			'imgfile', 
			'imgx', 
			'imgy', 
			'imgw', 
			'imgh', 
			'imgtype', 
			'imglink'
		],
		'champs_versionnes' => [
			'titre', 
			'imgfile', 
			'imgx', 
			'imgy', 
			'imgw', 
			'imgh', 
			'imgtype', 
			'imglink'
		],
		'rechercher_champs' => [
			'titre' => 8
		],
		'rechercher_jointures' => [],
		'tables_jointures' => [
			'pdform_images_liens'
		]
	];

	return $tables;
}

function pdform_declarer_tables_auxiliaires($tables) {
	/**
	 * La définition de la table auxiliaire d'association des patrons.
	 */

	$tables['spip_pdform_patrons_liens'] = [
		'field' => [
			'id_pdform_patron' => 'bigint(21) DEFAULT "0" NOT NULL',
			'id_objet' => 'bigint(21) DEFAULT "0" NOT NULL',
			'objet' => 'VARCHAR(25) DEFAULT "" NOT NULL',
			'role' => 'VARCHAR(25) DEFAULT "" NOT NULL',
			'vu' => 'varchar(6) DEFAULT "non" NOT NULL'
		],
		'key' => [
			"PRIMARY KEY" => "id_pdform_patron,id_objet,objet,role",
			"KEY id_pdform_patron" => "id_pdform_patron",
			"KEY id_objet" => "id_objet",
			"KEY objet" => "objet",
			"KEY role" => "role"
		],
	];

	/**
	 * La définition de la table auxiliaire d'association des cellules.
	 * Une cellule pourra être associée à un ou plusieurs formulaires
	 * (cas des affichages similaires sur des formulaires quasi-identiques).
	 */

	$tables['spip_pdform_cellules_liens'] = [
		'field' => [
			'id_pdform_cellule' => 'bigint(21) DEFAULT "0" NOT NULL',
			'id_objet' => 'bigint(21) DEFAULT "0" NOT NULL',
			'objet' => 'VARCHAR(25) DEFAULT "" NOT NULL',
			'page' => "int(4) DEFAULT '1' NOT NULL", 
			'vu' => 'varchar(6) DEFAULT "non" NOT NULL',
		],
		'key' => [
			"PRIMARY KEY" => "id_pdform_cellule,id_objet,objet,page",
			"KEY id_pdform_cellule" => "id_pdform_cellule",
			"KEY id_objet" => "id_objet",
			"KEY objet" => "objet",
			"KEY page" => "page",
		],
	];

	/**
	 * La définition de la table auxiliaire d'association des images.
	 * ’rang_lien’ indique que la table est triable (function lien_triables dans action/editer_liens.php)
	 * L'ordre des liens génère l'ordre d'affichage et donc la superposition des images.
	 * ’page’ permet de préciser la page d'affichage
	 */
	
	$tables['spip_pdform_images_liens'] = [
		'field' => [
			'id_pdform_image' => 'bigint(21) DEFAULT "0" NOT NULL',
			'id_objet' => 'bigint(21) DEFAULT "0" NOT NULL',
			'objet' => 'VARCHAR(25) DEFAULT "" NOT NULL',
			'page' => "int(4) DEFAULT '1' NOT NULL", 
			'rang_lien' => "int(4) DEFAULT '0' NOT NULL", 
		],
		'key' => [
			"PRIMARY KEY" => "id_pdform_image,id_objet,objet,page",
			"KEY id_pdform_image" => "id_pdform_image",
			"KEY id_objet" => "id_objet",
			"KEY objet" => "objet",
			"KEY page" => "page",
		],
	];

	return $tables;
}
