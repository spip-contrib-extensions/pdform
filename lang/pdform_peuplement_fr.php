<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [
	'log_peuplement' => 'id_auteur=@id@ nom=@nom@ action:@mes@',
	'title_pdform_peuplement_exemples' => 'Des exemples pour savoir utiliser Pdform',
	'titre_pdform_peuplement_exemples' => 'Exemples',
	'titre_peuplement' => 'Peuplement d\'exemples',
];
