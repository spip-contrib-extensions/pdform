<?php

return [
    'pdformcellules' => 'Les Cellules',
    'pdformcellules_liaisons' => 'Associer des cellules à des patrons',
    'pdformimages' => 'Les Cadrages',
    'pdformimages_liaisons' => 'Associer des cadrages à des patrons',
	'pdformpatrons' => 'Les Patrons',
    'pdformpatrons_statuts' => 'Les statuts possibles',
    'pdformpages' => 'Les pages',
    'pdformpages_formats' => 'Les formats',
    'pdformpages_orientations' => 'L’orientation de la page',
    'pdformpages_pages' => 'Le nombre de pages',
    'pdformpeuplements' => 'Le peuplement',
    'pdformpeuplements_peuplements' => 'Les propositions',
    'pdformpeuplements_plugin' => 'Votre proposition',
	'pdformfontes' => 'L’écriture',
	'pdformfontes_polices' => 'Polices d’écriture',
	'pdformfontes_tailles' => 'Tailles',
	'pdformfontes_styles' => 'Styles',
];
