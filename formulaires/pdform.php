<?php 

/**
 * Formulaire d'exemple et de test des pdform_patrons
 *
 * @plugin     Pdform
 * @copyright  2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Pdform\Action
 */

if (!defined('_ECRIRE_INC_VERSION')) return;
 
 include_spip('inc/actions');
 include_spip('inc/editer');
 
 
	      
/*
 * Déclaration des champs du formulaire
 */
function formulaires_pdform_saisies_dist($id_pdform_patron=0,$ids_objets_nourriciers=0){

	# L'identifiant unique du patron

	if (!intval($id_pdform_patron)) {
		$pdform_patron = array(
			'saisie' => 'pdform_patrons',
			'options' => array(
				'nom' => 'id_pdform_patron',
				'label' =>  _T('pdform_patron:titre_pdform_patron'),
				'cacher_option_intro' => 'oui',
				'obligatoire' => 'oui',
			),
		);
	} else {
		$pdform_patron = array(
			'saisie' => 'hidden',
			'options' => array(
				'nom' => 'id_pdform_patron',
			),
		);
	}
	
	# Les identifiants uniques des objets nourriciers
	
	$objet_nourricier = array(
		'saisie' => 'input',
		'options' => array(
			'nom' => 'ids_objets_nourriciers',
			'label' => _T('pdform:champ_ids_objets_nourriciers_label'),
			'explication' => _T('pdform:champ_ids_objets_nourriciers_explication'),
		),
		'verifier' => array( // on vérifie que l'id transmis est un entier et que la graphie de l'objet est normale
			'type' => 'regex',
			'options' => array(
				'modele' => '#^[a-z_|0-9,]*$#',
			)
		)
	);
	
	# retourne la saisie

	$saisies = array(
		'options' => array(
			// Changer l'intitulé du bouton final de validation
			'texte_submit' => _T('pdform:bouton_valider'),
		),
		array( // le fieldset
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'editerpdf',
				'label' => _T('pdform:fieldset_editer'),
			),
			'saisies' => array( // les champs dans le fieldset 
				$pdform_patron,
				$objet_nourricier,
			),
		)
	);
	return $saisies;
}

/**
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 */
function formulaires_pdform_charger_dist($id_pdform_patron=0, $ids_objets_nourriciers=0, $retour=''){
	// charger les saisies
	$chargement = charger_fonction('pdform_saisies','formulaires');
	$valeurs = $chargement($id_pdform_patron, $ids_objets_nourriciers);
	/**
	 * Aider le spipeur en préremplissant les arguments utiles à la fonction de création d'un PDF
	 * et transmettant les ids des objets nourriciers. 
	 * Si les ids ne sont pas transmis, la fonction affichera
	 * les noms des clés à la place de leurs valeurs.
	 */
	$valeurs['id_pdform_patron'] = intval($id_pdform_patron);
	if ($ids_objets_nourriciers) {
		$valeurs['ids_objets_nourriciers'] = $ids_objets_nourriciers;
	} elseif ($valeurs['id_pdform_patron']) {
		if ($ids = sql_allfetsel('cell.table_nourriciere',
			'spip_pdform_cellules as cell INNER JOIN spip_pdform_cellules_liens as lien ON (cell.id_pdform_cellule=lien.id_pdform_cellule)', 
			array(
				'lien.id_objet=' . $valeurs['id_pdform_patron'],
				'lien.objet="pdform_patron"',
				'cell.table_nourriciere!=""',
			),
			array('table_nourriciere')
		)){
			$valeurs['ids_objets_nourriciers'] = '';
			foreach ($ids as $l) {
				$table_objet = $l['table_nourriciere'];
				$objet = objet_type($table_objet);
				$valeurs['ids_objets_nourriciers'] .= $objet.'|' . '1'.',';
			}
			$valeurs['ids_objets_nourriciers'] = substr($valeurs['ids_objets_nourriciers'], 0,-1);
		}
	}
	$valeurs['retour'] = $retour;
	return $valeurs;
}

/**
  * Traiter les champs postes
  */
function formulaires_pdform_traiter_dist($id_pdform_patron=0, $ids_objets_nourriciers=0, $retour=''){
	/**
	 * l'API pour créer un PDF nécessite :
	 * - un id_pdform_patron, identifiant uniique du patron utilisé pour créer le pdf.
	 * - un argument 'options' 'fichier_cache' contenant le chemin et nom du fichier pdf à créer.
	 * 
	 * Ces informations peuvent être accompagnée des ids des objets que le patron traitera.
	 *
	 * Les arguments transmis dans le flot vous permettent d'ajouter des informations
	 * à celle que le patron ira chercher dans les champs éditables des objets déclarés.
	 * Au-delà des ’options’ (dont la nécessaire définition du nom du fichier pdf),
	 * on y distingue les ’arguments’, qui ajoutent des clés, étendant les possibilités
	 * d'affichage du patron à des données transmmises autrement que par les objets SPIP.
	 * Les ’formes’ qui permettent d'afficher des formes.
	 * Les ’textes’, défini par pages.
	 *
	 * Le tableau ci-dessous ($flot) sera abondé d'exemples avec le patron 'Exemples d\'utilisations'
	 * qui peut être installé depuis la colonne afficher_gauche de la liste des patrons.
	 */
	
	include_spip('inc/pdform_creer');

	// Arguments nécessaires

	$id_pdform_patron = _request('id_pdform_patron');
	$ids =_request('ids_objets_nourriciers');
	$objets_nourriciers = '';
	// verifier les $ids et les mettre en tableau
	if ($ids){
		// on formate les $ids
		if (!$erreur = pdform_verifier_ids($ids)){
			return array(
				'editable' => true,
				'message_erreur' => $erreur,
			);
		}
		// transformer le tableau des $ids en string simple pour le nom du fichier
		foreach ($ids as $key => $value) {
			$objets_nourriciers .= $key . $value;
		}
	}

	// determination du nom du fichier pdf avec ezcache

	$cache = array(
		'id_pdform_patron' => $id_pdform_patron,
		'objets_nourriciers' => $objets_nourriciers
	);
	$plugin = 'pdform'; $type = 'pdf';
	include_spip('inc/ezcache_cache');
	// on calcul le chemin du cache car de toute façon il faut le renvoyer
	$fichier_cache = cache_nommer($plugin, $type, $cache);
	// si le cache existe, il n'est pas nécessaire de créer le pdf
	if ($retours = pdform_utiliser_cache($fichier_cache, generer_url_ecrire('pdform_patron', "id_pdform_patron=".$id_pdform_patron))){
		return $retours;
	}

	// On crée les répertoires si besoin car pdform_creer ne les créera pas 

	if (!is_dir(dirname($fichier_cache))) {
		$configuration = configuration_cache_lire($plugin, $type);
		// On crée les répertoires si besoin
		include_spip('inc/flock');
		$dir_cache = sous_repertoire(constant($configuration['racine']),rtrim($configuration['dossier_plugin'], '/'));
		if ($configuration['sous_dossier']) {
			sous_repertoire($dir_cache, rtrim($cache['sous_dossier'], '/'));
		}
	}
	
	// le flot

	if (sql_getfetsel("titre", "spip_pdform_patrons", "id_pdform_patron=" . intval($id_pdform_patron)) == 'Exemples d\'utilisations' ) {
		$flot = array(
			'options' => array(
				'fichier_cache' => $fichier_cache
			),
			'arguments' => array(
				'stamp_courrier' => 'now',
				'civilite_substitution' => '',
				'prenom_substitution' => '',
				'nom_substitution' => 'Picot de la Chevalière',
				'voie_substitution' => '3, rue de Chêne',
				'complement_substitution' => 'Maison du Gland',
				'code_postal_substitution' => '91 490',
				'ville_substitution' => 'Milly-la-Forêt',
				'bonjour' => 'Bonjour',
			),
	
				/**
				 * La définition de la fenêtre pour une enveloppe fenêtre est :
				 * un cadre de 3,3 cm de haut sur 8,5 cm de large. 
				 * Celui-ci se situe respectivement à 5,5 cm du bord supérieur
				 * de votre feuille et à 11 cm du bord gauche.
				 */
			'formes' => array(
				1 => array(
					'Rectangle_enveloppe_fenetre' => array(
						'type' => 'RoundedRect',
						'x' => '110',
						'y' => '55',
						'w' => '85',
						'h' => '33',
						'r' => '3.5',
						'style' => 'D',
						'SetLineWidth' => '0.1',
						//'SetFillColor' => array(50),
						//'SetDrawColor' => array(240),
					),
					'Rond1' => array(
						'type' => 'Circle',
						'x' => 55,
						'y' => '180',
						'r' => '1.01',
						'style' => 'FD',
						'SetLineWidth' => '0.2',
						'SetFillColor' => array(175,29,97),
						'SetDrawColor' => array(146,0,68),
					),
					'Rond2' => array(
						'type' => 'Circle',
						'x' => 60,
						'y' => '180',
						'r' => '1.01',
						'style' => 'FD',
						'SetLineWidth' => '0.2',
						'SetFillColor' => array(175,29,97),
						'SetDrawColor' => array(146,0,68),
					),
					'Rond3' => array(
						'type' => 'Circle',
						'x' => 65,
						'y' => '180',
						'r' => '1.01',
						'style' => 'FD',
						'SetLineWidth' => '0.2',
						'SetFillColor' => array(175,29,97),
						'SetDrawColor' => array(146,0,68),
					),
					'Rond4' => array(
						'type' => 'Circle',
						'x' => 70,
						'y' => '180',
						'r' => '1.01',
						'style' => 'FD',
						'SetLineWidth' => '0.2',
						'SetFillColor' => array(175,29,97),
						'SetDrawColor' => array(146,0,68),
					),
					'Oval1' => array(
						'type' => 'Ellipse',
						'x' => 75,
						'y' => '180',
						'rx' => '1.50',
						'ry' => '1.01',
						'style' => 'FD',
						'SetLineWidth' => '0.2',
						'SetFillColor' => array(175,29,97),
						'SetDrawColor' => array(146,0,68),
					),
					'Oval2' => array(
						'type' => 'Ellipse',
						'x' => 82,
						'y' => '180',
						'rx' => '3.00',
						'ry' => '1.01',
						'style' => 'FD',
						'SetLineWidth' => '0.2',
						'SetFillColor' => array(175,29,97),
						'SetDrawColor' => array(146,0,68),
					),
					'Oval3' => array(
						'type' => 'Ellipse',
						'x' => 95,
						'y' => '180',
						'rx' => '6.00',
						'ry' => '1.01',
						'style' => 'FD',
						'SetLineWidth' => '0.2',
						'SetFillColor' => array(175,29,97),
						'SetDrawColor' => array(146,0,68),
					),
				),
			),
			'textes' => array(
				1 => array(
					'salutation' => array(
						'x' => '120',
						'y' => '140',
						'h' => '5',
						'txt' => "Coucou l'écureuil ! Pense à venir avec tes glands, non pas parce que nous sommes des glandeurs mais parce que SPIP, c'est contributif et on partage nos ressources !",
						'link' => '',
					),
				),
				3 => array(
					'salutation' => array(
						'x' => '10',
						'y' => '10',
						'h' => '5',
						'txt' => "Sur cette page blanche, avec le flot du plugin, je place ce que je veux...",
						'link' => '',
					),
				),
			),
			'images' => array(
				3 => array(
					'imgfile' => 'exemples/accessoires/spip_pastille_256x256.png',
					'imgx' => '10',
					'imgy' => '25',
					'imgw' => '25',
					'imgh' => '25',
				), array(
					'imgfile' => 'exemples/accessoires/spip_pastille_256x256.png',
					'imgx' => '30',
					'imgy' => '50',
					'imgw' => '25',
					'imgh' => '25',
				), array(
					'imgfile' => 'exemples/accessoires/spip_pastille_256x256.png',
					'imgx' => '50',
					'imgy' => '75',
					'imgw' => '25',
					'imgh' => '25',
				), array(
					'imgfile' => 'exemples/accessoires/spip_pastille_256x256.png',
					'imgx' => '70',
					'imgy' => '100',
					'imgw' => '25',
					'imgh' => '25',
				),
			),
		);
	} else {
		$flot = array(
			'options' => array(
				'fichier_cache' => $fichier_cache
			)
		);
	}
	

	$ids =_request('ids_objets_nourriciers');
	if (is_string($ids) and strlen($ids)) {
		$ids=explode(',', $ids);
	}

	if ($id_patron = intval(_request('id_pdform_patron'))) {
		
		$retours = pdform_creer($id_patron, $ids, $flot);
	}
	$retours['editable'] = true; // le formulaire peut être utilisé plusieurs fois à la suite.
	if ($retour) {
	$retours['redirect'] = $retour; // redirection après les traitements du formulaire. Par défaut la page boucle sur elle-même.
	}
	return $retours;
}