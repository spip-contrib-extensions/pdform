<?php 

/**
 * Formulaire pour créer une cellule
 * (des champs textes s'affichant dans un rectangle)
 */
if (!defined('_ECRIRE_INC_VERSION')) return;
 
 include_spip('inc/actions');
 include_spip('inc/editer');
      
/**
 * Déclaration des champs du formulaire
 *
 * Références des vérifications :
 * @link: https://contrib.spip.net/References-des-verifications
 */
function formulaires_editer_pdform_cellule_saisies_dist($id_pdform_cellule='new', $retour='', $associer_objet = '', $lier_trad=0, $config_fonc='', $row=[], $hidden=''){
	$id_pdform_cellule = (int) $id_pdform_cellule;

	$saisies[] = [ // le fieldset de la table nourricière éventuelle
		'saisie' => 'fieldset',
		'options' => [
			'nom' => 'fieldset_champ_editable',
			'label' => '<:pdform_cellule:fieldset_champ_editable_label:>',
			'explication' => '<:pdform_cellule:fieldset_champ_editable_explication:>',
		],
		'saisies' => [ // les champs dans le fieldset 
			[
			'saisie' => 'hidden', // l'identifiant unique de la cellule
			'options' => [
				'nom' => 'id_pdform_cellule',
				'defaut' => $id_pdform_cellule,
				],
			],
			[
				'saisie' => 'tables_nourricieres', // l'objet dont le champ éditable provient
				'options' => [
					'nom' => 'table_nourriciere',
					'label' => '<:pdform_cellule:champ_table_nourriciere_label:>',
					'explication' => '<:pdform_cellule:champ_table_nourriciere_explication:>',
					'afficher_icone' => 'oui',
					#'select' => 'oui',
				],
			],
			[
				'saisie' => 'input',
				'options' => [
					'nom' => 'titre',
					'label' => '<:pdform_cellule:champ_titre_label:>',
					'explication' => '<:pdform_cellule:champ_titre_explication:>',
				],
			],
			'verifier' => [ // le nombre est de type float
				'type' => 'taille',
				'options' => [
					'maxi' => '255',
					'min' => '0',
				],
			],
			[
				'saisie' => 'textarea',
				'options' => [
					'nom' => 'texte',
					'label' => '<:pdform_cellule:champ_celltxt_label:>',
					'explication' => '<:pdform_cellule:champ_celltxt_explication:>',
				],
			],
		],
	];
		
	$saisies[] = [ // le fieldset cell
		'saisie' => 'fieldset',
		'options' => [
			'nom' => 'fieldset_cell',
			'label' => '<:pdform_cellule:fieldset_cell_label:>',
			'explication' => '<:pdform_cellule:fieldset_cell_explication:>',
		],
		'saisies' => [ // les champs dans le fieldset 
			[
				'saisie' => 'pdform_polices',
				'options' => [
					'nom' => 'police',
					'label' => '<:pdform_patron:champ_police_label:>',
					'aide' => 'pdformfontes/polices',
				],
			],
			[
				'saisie' => 'input',
				'options' => [
					'nom' => 'taille',
					'label' => '<:pdform_patron:champ_taille_label:>',
					'aide' => 'pdformfontes/tailles',
				],
			],
			[
				'saisie' => 'selection',
				'options' => [
					'nom' => 'style',
					'label' => '<:pdform_patron:champ_style_label:>',
					'aide' => 'pdformfontes/styles',
					'cacher_option_intro' => 'oui',
					'data' => [
						'' => '<:pdform_patron:data_fontstyle_:>',
						'B' => '<:pdform_patron:data_fontstyle_b:>',
						'U' => '<:pdform_patron:data_fontstyle_u:>',
						'I' => '<:pdform_patron:data_fontstyle_i:>',
					],
				],
			],
			[ // couleur d'affichage de la police
				'saisie' => 'couleur',
				'options' => [
					'nom' => 'couleur_texte',
					'label' => '<:pdform_patron:champ_couleur_texte_label:>',
					'explication' => '<:pdform_patron:champ_couleur_texte_explication:>',
				],
			],
			'verifier' => [ // la valeur est une couleur en hexa 
				'type' => 'couleur',
				'options' => [
					'type' => 'hexa',
					'normaliser' => 'oui'
				],
			],
			[
				'saisie' => 'input',
				'options' => [
					'nom' => 'x',
					'label' => '<:pdform_patron:champ_x_label:>',
					'explication' => '<:pdform_patron:champ_x_explication:>',
				],
			],
			'verifier' => [ // le nombre est de type float
				'type' => 'decimal',
				'options' => [
					'maxi' => '1000',
					'min' => '0',
					'nb_decimales' => '2',
					'normaliser' => 'oui',
					'separateur' => '.',
				],
			],
			[
				'saisie' => 'input',
				'options' => [
					'nom' => 'y',
					'label' => '<:pdform_patron:champ_y_label:>',
					'explication' => '<:pdform_patron:champ_y_explication:>',
				],
			],
			'verifier' => [ // le nombre est de type float
				'type' => 'decimal',
				'options' => [
					'maxi' => '1000',
					'min' => '0',
					'nb_decimales' => '2',
					'normaliser' => 'oui',
					'separateur' => '.',
				],
			],
			[
				'saisie' => 'input',
				'options' => [
					'nom' => 'largeur',
					'label' => '<:pdform_patron:champ_largeur_label:>',
					'explication' => '<:pdform_patron:champ_largeur_explication:>',
				],
			],
			'verifier' => [ // le nombre est de type float
				'type' => 'decimal',
				'options' => [
					'maxi' => '1000',
					'min' => '0',
					'nb_decimales' => '2',
					'normaliser' => 'oui',
					'separateur' => '.',
				],
			],
			[
				'saisie' => 'input',
				'options' => [
					'nom' => 'hauteur',
					'label' => '<:pdform_patron:champ_hauteur_label:>',
					'explication' => '<:pdform_patron:champ_hauteur_explication:>',
					'defaut' => '5',
				],
			],
			'verifier' => [ // le nombre est de type float
				'type' => 'decimal',
				'options' => [
					'maxi' => '1000',
					'min' => '0',
					'nb_decimales' => '2',
					'normaliser' => 'oui',
					'separateur' => '.',
				],
			],
			[
				'saisie' => 'selection',
				'options' => [
					'nom' => 'bordure',
					'label' => '<:pdform_patron:champ_bordure_label:>',
					'explication' => '<:pdform_patron:champ_bordure_explication:>',
					'cacher_option_intro' => 'oui',
					#'multiple' => 'non',
					'data' => [
						'0' => '<:pdform_patron:data_border_0:>', // aucun bord tracé
						'1' => '<:pdform_patron:data_border_1:>', // un cadre doit être tracé
						'L' => '<:pdform_patron:data_border_l:>', // un bord gauche doit être tracé
						'T' => '<:pdform_patron:data_border_t:>', // un bord haut doit être tracé
						'R' => '<:pdform_patron:data_border_r:>', // un bord droit doit être tracé
						'B' => '<:pdform_patron:data_border_b:>', // un bord bas doit être tracé
					],
				],
			],
			'verifier' => [ // les valeurs peuvent être 0|1|L|T|R|B
				'type' => 'regex',
				'options' => [
					'modele' => '@^[0-1L|T|R|B]$@',
					'message_erreur' => '<:pdform_patron:message_erreur_regex_border:>',
				],
			],
			[ // Fixe la couleur des bords de la cellule
				'saisie' => 'couleur',
				'options' => [
					'nom' => 'couleur_trace',
					'label' => '<:pdform_patron:champ_couleur_trace_label:>',
					'explication' => '<:pdform_cellule:champ_couleur_trace_explication:>',
					'afficher_si' => '@bordure@ != 0',
				],
			],
			'verifier' => [ // tous les types d'url
				'type' => 'couleur',
				'options' => [
					'type' => 'hexa',
					'normaliser' => 'oui'
				],
			],
			[
				'saisie' => 'case',
				'options' => [
					'nom' => 'plein',
					'label' => '<:pdform_patron:champ_rempli_label:>',
					'explication' => '<:pdform_patron:champ_rempli_explication:>',
					'valeur_oui' => '1',
					'valeur_non' => '0',
				],
			],
			[ // Fixe la couleur de remplissage du fond de la cellule
				'saisie' => 'couleur',
				'options' => [
					'nom' => 'couleur_rempli',
					'label' => '<:pdform_patron:champ_couleur_rempli_label:>',
					'explication' => '<:pdform_patron:champ_couleur_rempli_explication:>',
					'afficher_si' => '@plein@ == 1',
				],
			],
			'verifier' => [ // tous les types d'url
				'type' => 'couleur',
				'options' => [
					'type' => 'hexa',
					'normaliser' => 'oui'
				],
			],
			[
				'saisie' => 'selection',
				'options' => [
					'nom' => 'apres',
					'label' => '<:pdform_patron:champ_apres_label:>',
					'explication' => '<:pdform_patron:champ_apres_explication:>',
					'cacher_option_intro' => 'oui',
					'defaut' => '2', // en dessous (utile si cell avec des retours à la ligne)
					'data' => [
						'0' => '<:pdform_patron:data_ln_0:>',
						'1' => '<:pdform_patron:data_ln_1:>',
						'2' => '<:pdform_patron:data_ln_2:>',
					],
				],
			],
			'verifier' => [ // les valeurs peuvent être 0|1|2
				'type' => 'regex',
				'options' => [
					'modele' => '@^[0-2]$@',
					'message_erreur' => '<:pdform_patron:message_erreur_regex_apres:>',
				],
			],
			[
				'saisie' => 'selection',
				'options' => [
					'nom' => 'align',
					'label' => '<:pdform_patron:champ_align_label:>',
					'explication' => '<:pdform_patron:champ_align_explication:>',
					'cacher_option_intro' => 'oui',
					#'multiple' => 'non',
					'data' => [
						'L' => '<:pdform_patron:data_align_l:>',
						'C' => '<:pdform_patron:data_align_c:>',
						'R' => '<:pdform_patron:data_align_r:>',
					],
				],
			],
			'verifier' => [ // la valeur peuvent être L|C|R
				'type' => 'regex',
				'options' => [
					'modele' => '@^[L|C|R]$@',
					'message_erreur' => '<:pdform_patron:message_erreur_regex_align:>',
				],
			],
			[
				'saisie' => 'input',
				'options' => [
					'nom' => 'lien',
					'label' => '<:pdform_patron:champ_lien_label:>',
					'explication' => '<:pdform_patron:champ_lien_explication:>',
				],
			],
			'verifier' => [ // tous les types d'url
				'type' => 'url',
			],
		],
	];

	if ($associer_objet){
		include_spip('inc/filtres'); // fonctions pour les objets (necessaire en cas d'association)
		$saisies[] = [ // le fieldset d'association
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'association',
				'label' => _T('pdform_cellule:fieldset_association'),
				#'icone' => $icone_objet,
			],
			'saisies' => [ // les champs dans le fieldset 
				[
					'saisie' => 'case',
					'options' => [
						'nom' => 'c_associe',
						'label' => _T('pdform_cellule:texte_associer_cellule_a', 
							[
								'str' => _T(strstr($associer_objet,'|',true) . ':titre_' . strstr($associer_objet,'|',true)),
								'nb' => substr(strstr(_request('associer_objet'),'|'),1)
							]
						),
						'valeur_oui' => 'oui', 
						'valeur_non' => 'non',
						'defaut' => 'oui',
					]
				],
			]
		];
	}

	return $saisies;
}

/**
  * Identifier le formulaire en faisant abstraction des parametres qui ne representent pas l'objet edite
  */
function formulaires_editer_pdform_cellule_identifier_dist($id_pdform_cellule='new', $retour='', $associer_objet = '', $lier_trad=0, $config_fonc='', $row=[], $hidden=''){
	return serialize(array(intval($id_pdform_cellule), $associer_objet));
}
 
/**
  * Declarer les champs postes et y integrer les valeurs par defaut
  */
function formulaires_editer_pdform_cellule_charger_dist($id_pdform_cellule='new', $retour='', $associer_objet = '', $lier_trad=0, $config_fonc='', $row=[], $hidden=''){
	// Initialisation des valeurs à transmettre au formulaire
	$valeurs = [];

	$valeurs = formulaires_editer_objet_charger('pdform_cellule',$id_pdform_cellule,'',$lier_trad,$retour,$config_fonc,$row,$hidden);

	# une cellule doit avoir une table_nourriciere

	# en création l'information doit être transmise par l'url
	if (!intval($id_pdform_cellule) and !$valeurs['table_nourriciere']){
		$valeurs['table_nourriciere'] = _request('table_nourriciere');
	}
	
	# Une association 
	$valeurs['associer_objet'] = $associer_objet ? $associer_objet : _request('associer_objet');
	if ($valeurs['associer_objet']) {
		$valeurs['c_associer'] = 'oui';
	}

	# normaliser en hexa le code rgb
	$couleurs = [
		'couleur_trace' => $valeurs['couleur_trace'],
		'couleur_rempli' => $valeurs['couleur_rempli'],
		'couleur_texte' => $valeurs['couleur_texte'],
	];
	$pdfrom_couleurs = charger_fonction('pdform_couleurs', 'inc');
	foreach ($couleurs as $colonne => $rgb) {
		if ($rgb){
			$hexa = $pdfrom_couleurs($rgb);
			if ($hexa){
				$valeurs[$colonne] = $hexa;
			}
		}
	}
	return $valeurs;
}

/**
  * Verifier les champs postes et signaler d'eventuelles erreurs
  */
 function formulaires_editer_pdform_cellule_verifier_dist($id_pdform_cellule='new', $retour='', $associer_objet = '', $lier_trad=0, $config_fonc='', $row=[], $hidden=''){
	$erreurs = [];//formulaires_editer_objet_verifier('pdform_cellule',$id_pdform_cellule);

	return $erreurs;
}

/**
  * Traiter les champs postes
  */
function formulaires_editer_pdform_cellule_traiter_dist($id_pdform_cellule='new', $retour='', $associer_objet = '', $lier_trad=0, $config_fonc='', $row=[], $hidden=''){

	# normaliser en rgb le code en hexa
	$couleurs = [
		'couleur_trace' => _request('couleur_trace'),
		'couleur_rempli' => _request('couleur_rempli'),
		'couleur_texte' => _request('couleur_texte'),
	];
	$pdfrom_couleurs = charger_fonction('pdform_couleurs', 'inc');
	foreach ($couleurs as $colonne => $hexa) {
		$rgb = $pdfrom_couleurs($hexa);
		if ($rgb){
			set_request($colonne, $rgb);
		}
	}

	// rompre l'association si nécessaire
	if (_request('c_associe') == 'non'){
		$associer_objet = '';
	}

	$retours = formulaires_editer_objet_traiter('pdform_cellule',$id_pdform_cellule,'',$lier_trad,$retour,$config_fonc,$row,$hidden);

	// Un lien a prendre en compte ?
	if ($associer_objet and $id_pdform_cellule = $retours['id_pdform_cellule']) {
		list($objet, $id_objet) = explode('|', $associer_objet);
		if ($objet and $id_objet == intval($id_objet)) {
			include_spip('action/editer_liens');
			objet_associer(
				['pdform_cellule' => $id_pdform_cellule],
				[$objet => $id_objet]
			);
			// rediriger vers l'objet
			if (isset($redirect)) {
				$retours['redirect'] = parametre_url($redirect, 'id_lien_ajoute', $id_pdform_cellule, '&');
			}
		}
	}
	return $retours;
}