<?php 


if (!defined('_ECRIRE_INC_VERSION')) return;
 
 include_spip('inc/actions');
 include_spip('inc/editer');
 
 
	      
/*
  * Déclaration des champs du formulaire
  */
function formulaires_editer_pdform_patron_saisies_dist($id_pdform_patron='new', $retour='', $associer_objet = '', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$id_pdform = intval($id_pdform_patron);

	$saisies = array(
		array( // le fieldset racine général
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'general',
				'label' => _T('pdform_patron:fieldset_global'),
			),
			'saisies' => array( // les champs dans le fieldset 
				array(
					'saisie' => 'hidden', // l'association éventuelle
					'options' => array(
						'nom' => 'associer_objet',
						'defaut' => $associer_objet,
					),
				),
				array(
					'saisie' => 'input', // titre
					'options' => array(
						'nom' => 'titre',
						'label' => _T('pdform_patron:champ_titre_label'),
						'explication' => _T('pdform_patron:champ_titre_explication'),
					),
				),
				'verifier' => array(
					'type' => 'taille',
					'options' => array(
						'maxi' => '255',
					),
				),
				array(
					'saisie' => 'selection',
					'options' => array(
						'nom' => 'orientation',
						'label' => _T('pdform_patron:champ_orientation_label'),
						'aide' => 'pdformpages/orientations',
						'cacher_option_intro' => 'oui', // Il est obligatoire d'avoir une orientation
						'defaut' => 'P',
						'data' => array(
							'P' => _T('pdform_patron:data_orientation_p'),
							'L' => _T('pdform_patron:data_orientation_l')
						)
					),
				),
				array(
					'saisie' => 'selection',
					'options' => array(
						'aide' => 'pdformpages/formats',
						'nom' => 'format',
						'valeur_forcee' => 'A4',
						'label' => _T('pdform_patron:champ_format_label'),
						'cacher_option_intro' => 'oui', // Il est obligatoire d'avoir un format
						'data' => array(
							'A10' => _T('pdform_patron:data_format_a10'),
							'A9' => _T('pdform_patron:data_format_a9'),
							'A8' => _T('pdform_patron:data_format_a8'),
							'A7' => _T('pdform_patron:data_format_a7'),
							'A6' => _T('pdform_patron:data_format_a6'),
							'A5' => _T('pdform_patron:data_format_a5'),
							'A4' => _T('pdform_patron:data_format_a4'),
							'A3' => _T('pdform_patron:data_format_a3'),
							'A2' => _T('pdform_patron:data_format_a2'),
							'A1' => _T('pdform_patron:data_format_a1'),
							'A0' => _T('pdform_patron:data_format_a0'),
						),
					),
				),
				array(
					'saisie' => 'input', // pages
					'options' => array(
						'nom' => 'pages',
						'aide' => 'pdformpages/pages',
						'label' => _T('pdform_patron:champ_pages_label'),
						'defaut' => '1',
					),
				),
				array(
					'saisie' => 'pdform_polices',
					'options' => array(
						'nom' => 'police',
						'label' => _T('pdform_patron:champ_police_label'),
						'aide' => 'pdformfontes/polices',
						'cacher_option_intro' => 'oui', 
						'defaut' => 'helvetica',
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'taille',
						'label' => _T('pdform_patron:champ_taille_label'),
						'aide' => 'pdformfontes/tailles',
						'defaut' => '12',
					),
				),
				array(
					'saisie' => 'selection',
					'options' => array(
						'nom' => 'style',
						'label' => _T('pdform_patron:champ_style_label'),
						'aide' => 'pdformfontes/styles',
						'cacher_option_intro' => 'oui',
						'defaut' => '',
						'data' => array(
							'' => _T('pdform_patron:data_fontstyle_'),
							'B' => _T('pdform_patron:data_fontstyle_b'),
							'U' => _T('pdform_patron:data_fontstyle_u'),
							'I' => _T('pdform_patron:data_fontstyle_i'),
						),
					),
				),
				array(
					'saisie' => 'couleur',
					'options' => array(
						'nom' => 'couleur_texte',
						'label' => _T('pdform_patron:champ_couleur_texte_label'),
						'explication' => _T('pdform_patron:champ_couleur_texte_explication'),
					),
				),
				'verifier' => array( // tous les types d'url
					'type' => 'couleur',	
					'options' => array(
						'type' => 'hexa',
						'normaliser' => 'oui'
					),
				),
				array(
					'saisie' => 'couleur',
					'options' => array(
						'nom' => 'couleur_trace',
						'label' => _T('pdform_patron:champ_couleur_trace_label'),
						'explication' => _T('pdform_patron:champ_couleur_trace_explication'),
					),
				),
				'verifier' => array( // tous les types d'url
					'type' => 'couleur',	
					'options' => array(
						'type' => 'hexa',
						'normaliser' => 'oui'
					),
				),
				array(
					'saisie' => 'couleur',
					'options' => array(
						'nom' => 'couleur_rempli',
						'label' => _T('pdform_patron:champ_couleur_rempli_label'),
						'explication' => _T('pdform_patron:champ_couleur_rempli_explication'),
					),
				),
				'verifier' => array( // tous les types d'url
					'type' => 'couleur',	
					'options' => array(
						'type' => 'hexa',
						'normaliser' => 'oui'
					),
				),
			),
		),
		array( // le fieldset de la définition des en-têtes
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'etape_1',
				'label' => _T('pdform_patron:fieldset_entete'),
				'pliable' => 'oui',
				'plie' => 'oui'
			),
			'saisies' => array( // les champs dans le fieldset 
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'entete_texte',
						'label' => _T('pdform_patron:champ_texte_label'),
						'explication' => _T('pdform_patron:champ_entete_texte_explication'),
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'entete_pages',
						'label' => _T('pdform_patron:champ_pages_label'),
						'explication' => _T('pdform_patron:champ_entete_pages_explication'),
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'entete_x',
						'label' => _T('pdform_patron:champ_x_label'),
						'explication' => _T('pdform_patron:champ_x_explication'),
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'entete_y',
						'label' => _T('pdform_patron:champ_y_label'),
						'explication' => _T('pdform_patron:champ_y_explication'),
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'entete_largeur',
						'label' => _T('pdform_patron:champ_largeur_label'),
						'explication' => _T('pdform_patron:champ_largeur_explication'),
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'entete_hauteur',
						'label' => _T('pdform_patron:champ_hauteur_label'),
						'explication' => _T('pdform_patron:champ_hauteur_explication'),
					),
				),
				array(
					'saisie' => 'pdform_polices',
					'options' => array(
						'nom' => 'entete_police',
						'aide' => 'pdformfontes/polices',
						'label' => _T('pdform_patron:champ_police_label'),
						#'cacher_option_intro' => 'oui',
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'entete_taille',
						'label' => _T('pdform_patron:champ_taille_label'),
						'aide' => 'pdformfontes/tailles',
					),
				),
				array(
					'saisie' => 'selection',
					'options' => array(
						'nom' => 'entete_style',
						'label' => _T('pdform_patron:champ_style_label'),
						'aide' => 'pdformfontes/styles',
						'cacher_option_intro' => 'oui',
						'data' => array(
							'' => _T('pdform_patron:data_fontstyle_'),
							'B' => _T('pdform_patron:data_fontstyle_b'),
							'U' => _T('pdform_patron:data_fontstyle_u'),
							'I' => _T('pdform_patron:data_fontstyle_i'),
						),
					),
				),
				array(
					'saisie' => 'couleur',
					'options' => array(
						'nom' => 'entete_couleur_texte',
						'label' => _T('pdform_patron:champ_couleur_texte_label'),
						'explication' => _T('pdform_patron:champ_couleur_texte_explication'),
					),
				),
				'verifier' => array( // tous les types d'url
					'type' => 'couleur',
					'options' => array(
						'type' => 'hexa',
						'normaliser' => 'oui'
					),
				),
				array(
					'saisie' => 'selection',
					'options' => array(
						'nom' => 'entete_bordure',
						'label' => _T('pdform_patron:champ_bordure_label'),
						'explication' => _T('pdform_patron:champ_bordure_explication'),
						'cacher_option_intro' => 'oui',
						#'multiple' => 'non',
						'data' => array(
							'0' => _T('pdform_patron:data_border_0'),
							'1' => _T('pdform_patron:data_border_1'),
							'L' => _T('pdform_patron:data_border_l'),
							'T' => _T('pdform_patron:data_border_t'),
							'R' => _T('pdform_patron:data_border_r'),
							'B' => _T('pdform_patron:data_border_b'),
						),
					),
				),
				'verifier' => array( // les valeurs peuvent être 0|1|L|T|R|B
					'type' => 'regex',
					'options' => array(
						'modele' => '@^[0-1L|T|R|B]$@',
						'message_erreur' => _T('pdform_patron:message_erreur_regex_bordure'),
					),
				),
				array(
					'saisie' => 'couleur',
					'options' => array(
						'nom' => 'entete_couleur_trace',
						'label' => _T('pdform_patron:champ_couleur_trace_label'),
						'explication' => _T('pdform_patron:champ_couleur_entete_bord_explication'),
						'afficher_si' => '@entete_bordure@ != 0',
					),
				),
				'verifier' => array( // tous les types d'url
					'type' => 'couleur',
					'options' => array(
						'type' => 'hexa',
						'normaliser' => 'oui'
					),
				),
				array(
					'saisie' => 'case',
					'options' => array(
						'nom' => 'entete_plein',
						'label' => _T('pdform_patron:champ_rempli_label'),
						'explication' => _T('pdform_patron:champ_rempli_explication'),
						'valeur_oui' => '1',
						'valeur_non' => '0',
					),
				),
				array(
					'saisie' => 'couleur',
					'options' => array(
						'nom' => 'entete_couleur_rempli',
						'label' => _T('pdform_patron:champ_couleur_rempli_label'),
						'explication' => _T('pdform_patron:champ_couleur_entete_rempli_explication'),
						'afficher_si' => '@entete_plein@ == 1',
					),
				),
				'verifier' => array( // tous les types d'url
					'type' => 'couleur',
					'options' => array(
						'type' => 'hexa',
						'normaliser' => 'oui'
					),
				),
				array(
					'saisie' => 'selection',
					'options' => array(
						'nom' => 'entete_apres',
						'label' => _T('pdform_patron:champ_apres_label'),
						'explication' => _T('pdform_patron:champ_apres_explication'),
						'cacher_option_intro' => 'oui',
						'defaut' => '2', // en dessous (utile si cell avec des retours à la ligne)
						'data' => array(
							'0' => _T('pdform_patron:data_ln_0'),
							'1' => _T('pdform_patron:data_ln_1'),
							'2' => _T('pdform_patron:data_ln_2'),
						),
					),
				),
				'verifier' => array( // les valeurs peuvent être 0|1|2
					'type' => 'regex',
					'options' => array(
						'modele' => '@^[0-2]$@',
						'message_erreur' => _T('pdform_patron:message_erreur_regex_apres'),
					),
				),
				array(
					'saisie' => 'selection',
					'options' => array(
						'nom' => 'entete_align',
						'label' => _T('pdform_patron:champ_align_label'),
						'explication' => _T('pdform_patron:champ_align_explication'),
						'cacher_option_intro' => 'oui',
						#'multiple' => 'non',
						'data' => array(
							'L' => _T('pdform_patron:data_align_l'),
							'C' => _T('pdform_patron:data_align_c'),
							'R' => _T('pdform_patron:data_align_r'),
						),
					),
				),
				'verifier' => array( // la valeur peuvent être L|C|R
					'type' => 'regex',
					'options' => array(
						'modele' => '@^[L|C|R]$@',
						'message_erreur' => _T('pdform_patron:message_erreur_regex_align'),
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'entete_lien',
						'label' => _T('pdform_patron:champ_lien_label'),
						'explication' => _T('pdform_patron:champ_lien_explication'),
					),
				),
				'verifier' => array( // tous les types d'url
					'type' => 'url',
				),
			),
		),
		array( // le fieldset de la définition des pieds de page
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'pied',
				'label' => _T('pdform_patron:fieldset_pied'),
				'pliable' => 'oui',
				'plie' => 'oui'
			),
			'saisies' => array( // les champs dans le fieldset 
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'pied_texte',
						'label' => _T('pdform_patron:champ_texte_label'),
						'explication' => _T('pdform_patron:champ_pied_texte_explication'),
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'pied_pages',
						'label' => _T('pdform_patron:champ_pages_label'),
						'explication' => _T('pdform_patron:champ_entete_pages_explication'),
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'pied_x',
						'label' => _T('pdform_patron:champ_x_label'),
						'explication' => _T('pdform_patron:champ_y_explication'),
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'pied_y',
						'label' => _T('pdform_patron:champ_y_label'),
						'explication' => _T('pdform_patron:champ_y_explication'),
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'pied_largeur',
						'label' => _T('pdform_patron:champ_largeur_label'),
						'explication' => _T('pdform_patron:champ_largeur_explication'),
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'pied_hauteur',
						'label' => _T('pdform_patron:champ_hauteur_label'),
						'explication' => _T('pdform_patron:champ_hauteur_explication'),
					),
				),
				array(
					'saisie' => 'pdform_polices',
					'options' => array(
						'nom' => 'pied_police',
						'label' => _T('pdform_patron:champ_police_label'),
						'aide' => 'pdformfontes/polices',
						#'cacher_option_intro' => 'oui',
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'pied_taille',
						'label' => _T('pdform_patron:champ_taille_label'),
						'aide' => 'pdformfontes/tailles',
					),
				),
				array(
					'saisie' => 'selection',
					'options' => array(
						'nom' => 'pied_style',
						'label' => _T('pdform_patron:champ_style_label'),
						'aide' => 'pdformfontes/styles',
						'cacher_option_intro' => 'oui',
						'data' => array(
							'' => _T('pdform_patron:data_fontstyle_'),
							'B' => _T('pdform_patron:data_fontstyle_b'),
							'U' => _T('pdform_patron:data_fontstyle_u'),
							'I' => _T('pdform_patron:data_fontstyle_i'),
						),
					),
				),
				array(
					'saisie' => 'couleur',
					'options' => array(
						'nom' => 'pied_couleur_texte',
						'label' => _T('pdform_patron:champ_couleur_texte_label'),
						'explication' => _T('pdform_patron:champ_couleur_texte_explication'),
					),
				),
				'verifier' => array( // tous les types d'url
					'type' => 'couleur',
					'options' => array(
						'type' => 'hexa',
						'normaliser' => 'oui'
					),
				),
				array(
					'saisie' => 'selection',
					'options' => array(
						'nom' => 'pied_bordure',
						'label' => _T('pdform_patron:champ_bordure_label'),
						'explication' => _T('pdform_patron:champ_bordure_explication'),
						'cacher_option_intro' => 'oui',
						'data' => array(
							'0' => _T('pdform_patron:data_border_0'),
							'1' => _T('pdform_patron:data_border_1'),
							'L' => _T('pdform_patron:data_border_l'),
							'T' => _T('pdform_patron:data_border_t'),
							'R' => _T('pdform_patron:data_border_r'),
							'B' => _T('pdform_patron:data_border_b'),
						),
					),
				),
				'verifier' => array( // les valeurs peuvent être 0|1|L|T|R|B
					'type' => 'regex',
					'options' => array(
						'modele' => '@^[0-1L|T|R|B]$@',
						'message_erreur' => _T('pdform_patron:message_erreur_regex_bordure'),
					),
				),
				array(
					'saisie' => 'couleur',
					'options' => array(
						'nom' => 'pied_couleur_trace',
						'label' => _T('pdform_patron:champ_couleur_trace_label'),
						'explication' => _T('pdform_patron:champ_couleur_pied_bord_explication'),
						'afficher_si' => '@pied_bordure@ != 0'
					),
				),
				'verifier' => array( // tous les types d'url
					'type' => 'couleur',
					'options' => array(
						'type' => 'hexa',
						'normaliser' => 'oui'
					),
				),
				array(
					'saisie' => 'case',
					'options' => array(
						'nom' => 'pied_plein',
						'label' => _T('pdform_patron:champ_rempli_label'),
						'explication' => _T('pdform_patron:champ_rempli_explication'),
						'valeur_oui' => '1',
						'valeur_non' => '0',
					),
				),
				array(
					'saisie' => 'couleur',
					'options' => array(
						'nom' => 'pied_couleur_rempli',
						'label' => _T('pdform_patron:champ_couleur_rempli_label'),
						'explication' => _T('pdform_patron:champ_couleur_pied_rempli_explication'),
						'afficher_si' => '@pied_plein@ == 1'
					),
				),
				'verifier' => array( // tous les types d'url
					'type' => 'couleur',
					'options' => array(
						'type' => 'hexa',
						'normaliser' => 'oui'
					),
				),
				array(
					'saisie' => 'selection',
					'options' => array(
						'nom' => 'pied_apres',
						'label' => _T('pdform_patron:champ_apres_label'),
						'explication' => _T('pdform_patron:champ_apres_explication'),
						'cacher_option_intro' => 'oui',
						'defaut' => '2', // en dessous (utile si cell avec des retours à la ligne)
						'data' => array(
							'0' => _T('pdform_patron:data_ln_0'),
							'1' => _T('pdform_patron:data_ln_1'),
							'2' => _T('pdform_patron:data_ln_2'),
						),
					),
				),
				'verifier' => array( // les valeurs peuvent être 0|1|2
					'type' => 'regex',
					'options' => array(
						'modele' => '@^[0-2]$@',
						'message_erreur' => _T('pdform_patron:message_erreur_regex_apres'),
					),
				),
				array(
					'saisie' => 'selection',
					'options' => array(
						'nom' => 'pied_align',
						'label' => _T('pdform_patron:champ_align_label'),
						'explication' => _T('pdform_patron:champ_align_explication'),
						'cacher_option_intro' => 'oui',
						#'multiple' => 'non',
						'data' => array(
							'L' => _T('pdform_patron:data_align_l'),
							'C' => _T('pdform_patron:data_align_c'),
							'R' => _T('pdform_patron:data_align_r'),
						),
					),
				),
				'verifier' => array( // la valeur peuvent être L|C|R
					'type' => 'regex',
					'options' => array(
						'modele' => '@^[L|C|R]$@',
						'message_erreur' => _T('pdform_patron:message_erreur_regex_align'),
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'pied_lien',
						'label' => _T('pdform_patron:champ_lien_label'),
						'explication' => _T('pdform_patron:champ_lien_explication'),
					),
				),
				'verifier' => array( // tous les types d'url
					'type' => 'url',
				),
			),
		),
		array( // le fieldset d'association
			'saisie' => 'fieldset',
			'options' => array(
				'afficher_si'=> '@associer_objet@ != ""',
				'nom' => 'association',
				'label' => _T('pdform_patron:fieldset_association'),
				#'icone' => $icone_objet,
			),
			'saisies' => array( // les champs dans le fieldset 
				array(
					'saisie' => 'case',
					'options' => array(
						'nom' => 'c_associe',
						'label' => _T('pdform_patron:texte_associer_patron_a', array('str' => _T(strstr($associer_objet,'|',true).':titre_'.strstr($associer_objet,'|',true)), 'nb' => substr(strstr(_request('associer_objet'),'|'),1))),
						'valeur_oui' => 'oui', 
						'valeur_non' => 'non',
						'defaut' => 'oui',
					)
				),
			)
		)
	);
	
	return $saisies;
}

/**
  * Identifier le formulaire en faisant abstraction des parametres qui ne representent pas l'objet edite
  */
function formulaires_editer_pdform_patron_identifier_dist($id_pdform_patron='new', $retour='', $associer_objet = '', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	return serialize(array(intval($id_pdform_patron), $associer_objet));
}
 
/**
  * Declarer les champs postes et y integrer les valeurs par defaut
  */
function formulaires_editer_pdform_patron_charger_dist($id_pdform_patron='new', $retour='', $associer_objet = '', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$valeurs = formulaires_editer_objet_charger('pdform_patron',$id_pdform_patron,'',$lier_trad,$retour,$config_fonc,$row,$hidden);
	# normaliser en hexa le code rgb
	$couleurs = array(
		'couleur_trace' => $valeurs['couleur_trace'],
		'couleur_rempli' => $valeurs['couleur_rempli'],
		'couleur_texte' => $valeurs['couleur_texte'],
		'entete_couleur_trace' => $valeurs['entete_couleur_trace'],
		'entete_couleur_rempli' => $valeurs['entete_couleur_rempli'],
		'entete_couleur_texte' => $valeurs['entete_couleur_texte'],
		'pied_couleur_trace' => $valeurs['pied_couleur_trace'],
		'pied_couleur_rempli' => $valeurs['pied_couleur_rempli'],
		'pied_couleur_texte' => $valeurs['pied_couleur_texte'],
	);
	$pdfrom_couleurs = charger_fonction('pdform_couleurs', 'inc');
	foreach ($couleurs as $colonne => $rgb) {
		if ($rgb){
			$hexa = $pdfrom_couleurs($rgb);
			if ($hexa){
				$valeurs[$colonne] = $hexa;
			}
		}
	}

	return $valeurs;
}

/**
  * Verifier les champs postes et signaler d'eventuelles erreurs
  */
 function formulaires_editer_pdform_patron_verifier_dist($id_pdform_patron='new', $retour='', $associer_objet = '', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	 $erreurs = array();//formulaires_editer_objet_verifier('abonnement',$id_abonnement);
	return $erreurs;
}

/**
  * Traiter les champs postes
  */
function formulaires_editer_pdform_patron_traiter_dist($id_pdform_patron='new', $retour='', $associer_objet = '', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){

	# normaliser en rgb le code en hexa
	$couleurs = array(
		'couleur_trace' => _request('couleur_trace'),
		'couleur_rempli' => _request('couleur_rempli'),
		'couleur_texte' => _request('couleur_texte'),
		'entete_couleur_trace' => _request('entete_couleur_trace'),
		'entete_couleur_rempli' => _request('entete_couleur_rempli'),
		'entete_couleur_texte' => _request('entete_couleur_texte'),
		'pied_couleur_trace' => _request('pied_couleur_trace'),
		'pied_couleur_rempli' => _request('pied_couleur_rempli'),
		'pied_couleur_texte' => _request('pied_couleur_texte'),
	);
	$pdfrom_couleurs = charger_fonction('pdform_couleurs', 'inc');
	foreach ($couleurs as $colonne => $hexa) {
		$rgb = $pdfrom_couleurs($hexa);
		if ($rgb){
			set_request($colonne, $rgb);
		}
	}

	$associer_objet = _request('associer_objet');
	// rompre l'association si nécessaire
	if (_request('c_associe') == 'non'){
		$associer_objet = '';
	}

	$retours = formulaires_editer_objet_traiter('pdform_patron',$id_pdform_patron,'',$lier_trad,$retour,$config_fonc,$row,$hidden);

	// Un lien a prendre en compte ?
	if ($associer_objet and $id_pdform_patron = $retours['id_pdform_patron']) {
		list($objet, $id_objet) = explode('|', $associer_objet);
		if ($objet and $id_objet == intval($id_objet)) {
			include_spip('action/editer_liens');
			objet_associer(array('pdform_patron' => $id_pdform_patron), array($objet => $id_objet));
			// rediriger vers l'objet
			if (isset($redirect)) {
				$retours['redirect'] = parametre_url($redirect, 'id_lien_ajoute', $id_pdform_patron, '&');
			}
		}
	}

	return $retours;

}

?>