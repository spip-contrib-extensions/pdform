<?php
if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Un simple formulaire de config
 *
 * @link https://contrib.spip.net/Formulaire-de-configuration-avec-le-plugin-Saisies
 * 
 **/
function formulaires_configurer_pdform_saisies_dist(){
	# $saisies est un tableau décrivant les saisies à afficher dans le formulaire de configuration

	$saisies = [
		// indiquer les objets que l'on peut appeler
		[
			'saisie' => 'choisir_objets',
			'options' => [
				'nom' => 'tables_nourricieres',
				'label' => '<:pdform:cfg_tables_objets_label:>',
				'explication' => '<:pdform:cfg_tables_objets_explication:>',
				'exclus' => [
					'spip_pdform_patrons',
					'spip_pdform_cellules'
				],
			],
		],
		// indiquer si l'on veut le formulaire générique sur le contenu d'un patron
		[
			'saisie' => 'case',
			'options' => [
				'nom' => 'formulaire_sur_patron',
				'label_case' => '<:pdform:cfg_formulaire_sur_patron_label:>',
				'conteneur_class' => 'pleine_largeur'
			],
		],
		// indiquer si l'on veut des propositions de peuplement dans la colonne gauche de la liste des patrons
		[
			'saisie' => 'case',
			'options' => [
				'nom' => 'peuplement',
				'label_case' => '<:pdform:cfg_peuplement_label:>',
				'conteneur_class' => 'pleine_largeur'
			]
		]
	];
	return $saisies;
}
