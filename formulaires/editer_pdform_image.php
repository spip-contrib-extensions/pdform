<?php 

/**
 * Formulaire pour créer une image
 */
if (!defined('_ECRIRE_INC_VERSION')) return;
 
 include_spip('inc/actions');
 include_spip('inc/editer');
      
/**
 * Déclaration des champs du formulaire
 *
 * Références des vérifications :
 * @link: https://contrib.spip.net/References-des-verifications
 */
function formulaires_editer_pdform_image_saisies_dist($id_pdform_image='new', $retour='', $associer_objet = '', $lier_trad=0, $config_fonc='', $row=[], $hidden=''){
	$id_pdform_image = (int) $id_pdform_image;

	$saisies[] = [
		'saisie' => 'fieldset',
		'options' => [
			'nom' => 'fieldset_cadrage',
			'label' => _T('pdform_image:fieldset_champ_cadrage_label'),
			'explication' => _T('pdform_image:fieldset_champ_cadrage_explication'),
		],
		'saisies' => [ // les champs dans le fieldset 
			[
				'saisie' => 'hidden', // l'identifiant unique de la image
				'options' => [
					'nom' => 'id_pdform_image',
					'defaut' => $id_pdform_image,
				],
			],
			[
				'saisie' => 'input',
				'options' => [
					'nom' => 'titre',
					'label' => _T('pdform_image:champ_titre_label'),
					'explication' => _T('pdform_image:champ_titre_explication'),
				],
			],
			'verifier' => [ // le nombre est de type float
				'type' => 'taille',
				'options' => [
					'maxi' => '255',
					'min' => '0',
				],
			],
			[
				'saisie' => 'input',
				'options' => [
					'nom' => 'imgfile',
					'label' => _T('pdform_image:champ_imgfile_label'),
					'explication' => _T('pdform_image:champ_imgfile_explication'),
				],
			],
			'verifier' => [ // si c'est url web vérifier qu'elle existe
				'type' => 'url_exist',
				'options' => [
					'mode' => 'cool',
				]
			],
		],
	];

	$saisies[] = [ // le fieldset XY
		'saisie' => 'fieldset',
		'options' => [
			'nom' => 'fieldset_mesures',
			'label' => _T('pdform_image:fieldset_mesures_label'),
			'explication' => _T('pdform_image:fieldset_mesures_explication'),
		],
		'saisies' => [ // les champs dans le fieldset 
			[
				'saisie' => 'input',
				'options' => [
					'nom' => 'imgx',
					'label' => _T('pdform_image:champ_imgx_label'),
					#'explication' => _T('pdform_image:champ_setx_explication'),
				],
			],
			'verifier' => [ // le nombre est de type float
				'type' => 'decimal',
				'options' => [
					'maxi' => '1000',
					'min' => '0',
					'nb_decimales' => '2',
					'normaliser' => 'oui',
					'separateur' => '.',
				],
			],
			[
				'saisie' => 'input',
				'options' => [
					'nom' => 'imgy',
					'label' => _T('pdform_image:champ_imgy_label'),
					#'explication' => _T('pdform_image:champ_sety_explication'),
				],
			],
			'verifier' => [ // le nombre est de type float
				'type' => 'decimal',
				'options' => [
					'maxi' => '1000',
					'min' => '0',
					'nb_decimales' => '2',
					'normaliser' => 'oui',
					'separateur' => '.',
				],
			],
			[
				'saisie' => 'input',
				'options' => [
					'nom' => 'imgw',
					'label' => _T('pdform_image:champ_imgw_label'),
					'explication' => _T('pdform_image:champ_imgw_explication'),
				],
			],
			'verifier' => [ // le nombre est de type float
				'type' => 'decimal',
				'options' => [
					'maxi' => '1000',
					'min' => '0',
					'nb_decimales' => '2',
					'normaliser' => 'oui',
					'separateur' => '.',
				],
			],
			[
				'saisie' => 'input',
				'options' => [
					'nom' => 'imgh',
					'label' => _T('pdform_image:champ_imgh_label'),
					'explication' => _T('pdform_image:champ_imgh_explication'),
					'defaut' => '5',
				],
			],
			'verifier' => [ // le nombre est de type float
				'type' => 'decimal',
				'options' => [
					'maxi' => '1000',
					'min' => '0',
					'nb_decimales' => '2',
					'normaliser' => 'oui',
					'separateur' => '.',
				],
			],
			[
				'saisie' => 'selection',
				'options' => [
					'nom' => 'imgtype',
					'label' => '<:pdform_image:champ_imgtype_label:>',
					'explication' => '<:pdform_image:champ_imgtype_explication:>',
					'data' => _FPDF_FORMATS_IMG
				],
			],
			[
				'saisie' => 'input',
				'options' => [
					'nom' => 'imglink',
					'label' => _T('pdform_image:champ_imglink_label'),
					'explication' => _T('pdform_image:champ_imglink_explication'),
				],
			],
			'verifier' => [ // tous les types d'url
				'type' => 'url',
			],
		],
	];

	if ($associer_objet){
		include_spip('inc/filtres'); // fonctions pour les objets (necessaire en cas d'association)
		$saisies[] = [ // le fieldset d'association
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'association',
				'label' => _T('pdform_cellule:fieldset_association'),
				#'icone' => $icone_objet,
			],
			'saisies' => [ // les champs dans le fieldset 
				[ 
					'saisie' => 'case',
					'options' => [
						'nom' => 'c_associe',
						'label' => _T('pdform_image:texte_associer_image_a', 
							[ 
								'str' => _T(strstr($associer_objet,'|',true) . ':titre_' . strstr($associer_objet,'|',true)), 
								'nb' => substr(strstr(_request('associer_objet'),'|'),1)
							]
						),
						'valeur_oui' => 'oui', 
						'valeur_non' => 'non',
						'defaut' => 'oui',
					]
				],
			]
		];
	}
	return $saisies;
}

/**
  * Identifier le formulaire en faisant abstraction des parametres qui ne representent pas l'objet edite
  */
function formulaires_editer_pdform_image_identifier_dist($id_pdform_image='new', $retour='', $associer_objet = '', $lier_trad=0, $config_fonc='', $row=[], $hidden=''){
	return serialize([intval($id_pdform_image), $associer_objet]);
}
 
/**
  * Declarer les champs postes et y integrer les valeurs par defaut
  */
function formulaires_editer_pdform_image_charger_dist($id_pdform_image='new', $retour='', $associer_objet = '', $lier_trad=0, $config_fonc='', $row=[], $hidden=''){
	// Initialisation des valeurs à transmettre au formulaire
	$valeurs = [];

	$valeurs = formulaires_editer_objet_charger('pdform_image',$id_pdform_image,'',$lier_trad,$retour,$config_fonc,$row,$hidden);
	# Une association 
	$valeurs['associer_objet'] = $associer_objet ? $associer_objet : _request('associer_objet');
	if ($valeurs['associer_objet']) {
		$valeurs['c_associer'] = 'oui';
	}

	return $valeurs;
}


/**
  * Traiter les champs postes
  */
function formulaires_editer_pdform_image_traiter_dist($id_pdform_image='new', $retour='', $associer_objet = '', $lier_trad=0, $config_fonc='', $row=[], $hidden=''){

	// rompre l'association si nécessaire
	if (_request('c_associe') == 'non'){
		$associer_objet = '';
	}

	$retours = formulaires_editer_objet_traiter('pdform_image', $id_pdform_image, '', $lier_trad, $retour,$config_fonc, $row, $hidden);

	// Un lien a prendre en compte ?
	if ($associer_objet and $id_pdform_image = $retours['id_pdform_image']) {
		list($objet, $id_objet) = explode('|', $associer_objet);
		if ($objet and $id_objet == intval($id_objet)) {
			include_spip('action/editer_liens');
			objet_associer(
				['pdform_image' => $id_pdform_image],
				[$objet => $id_objet]
			);
			// rediriger vers l'objet
			if (isset($redirect)) {
				$retours['redirect'] = parametre_url($redirect, 'id_lien_ajoute', $id_pdform_image, '&');
			}
		}
	}
	return $retours;
}