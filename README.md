# PDForm

Outil pour créer un PDF, avec la librairie FPDF, à partir de différents objets de SPIP.
Dépot du plugin : https://git.spip.net/spip-contrib-extensions/pdform
Information sur la librairie FPDF : http://www.fpdf.org

## 1 opération pour une prise en main rapide

Installer l'exemple permet une prise en main rapide des fonctionnalités du plugin et de ses potentialités. Les exemples sont accessibles sur la gauche depuis la visualisations des patrons (Menu Edition | Patrons).

## 3 objets éditoriaux pour définir votre PDF

### Le Patron

Le Patron est l'objet principal du plugin accueillant par liaison des cellules et des cadrages.

Table : `spip_pdform_patrons` 
Nom de l'objet : Patron

### La Cellule

La Cellule permet de définir une zone de texte qui s'affichera sur le Patron auquel elle est associée. 
La Cellule peut automatiquement aller chercher les valeurs du champ d'un objet spip. Il faudra :
- déclarer l'objet (table nourricière) en le sélectionnant ;
- appeler le champ souhaité (texte) entre deux arobases (@).
La configuration du plugin vous permet de définir les Objets avec lesquels vous souhaitez que le plugin interagisse. Par défaut ce sera les auteurs.
Exemple : auteur @email@

Table : `spip_pdform_cellules`
Nom de l'objet : Cellule

### Le Cadrage

Le Cadrage permet de définir les conditions d'affichage d'une image sur le Patron auquel il est associé.

Table : `spip_pdform_images`
Nom de l'objet : Cadrage