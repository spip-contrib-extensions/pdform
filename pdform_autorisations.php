<?php
 /**
  * Plugin PDForm
  * (c) 2021 Vincent CALLIES
  * Licence GNU/GPL v3
  */
 
 if (!defined('_ECRIRE_INC_VERSION')) return;
 
 // declaration vide pour ce pipeline.
 function pdform_autoriser(){}
 
 /**
  * Trois objets éditoriaux : 
  * 'pdform_patrons'
  * 'pdform_cellules'
  * 'pdform_images'
  * Deux autorisations spéciales :
  * #AUTORISER{peuplement,pdform_patron}
  * #AUTORISER{production,pdform_patron}
  *
  * L'idée des autorisations telles qu'ici définies est que seuls les admins crées les patrons
  * et les admins peuvent avoir l'aide des admins restreints pour les ajuster (s'ils sont mals taillés)
  * tandis que les Rédacteurs sont des simples utilisateurs des formulaires.
  *
  * A vous, en surchargeant ces autorisations, de construire l'organisation de travail que vous souhaitez.
  * @link https://programmer.spip.net/Creer-ou-surcharger-des
  *
  * Note technique : l'underscore (tiret du bas) du nom des objets doit être retiré pour gérer les autorisations.
  * La production des formulaires pour vos utilisateurs peut se faire sur la base du formulaire :
  * /plugins/pdform/formulaires/pdform.php à adpater selon vos besoins.
  */
 
 // -----------------
 // Objet pdform_patrons
 
 // bouton de menu
 function autoriser_pdformpatrons_menu_dist($faire, $type, $id, $qui, $opts){
	 return $qui['statut'] == '0minirezo';
 } 

 // bouton de menu Outils rapides
function autoriser_pdformpatroncreer_menu_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('creer', 'pdformpatron', '', $qui, $opt);
}

 // creer
 function autoriser_pdformpatron_creer_dist($faire, $type, $id, $qui, $opt) {
	 return $qui['statut'] == '0minirezo' AND !$qui['restreint']; 
 }
 
 // voir les fiches completes
 function autoriser_pdformpatron_voir_dist($faire, $type, $id, $qui, $opt) {
 	return true;
 }
 
 // modifier
 function autoriser_pdformpatron_modifier_dist($faire, $type, $id, $qui, $opt) {
	 return $qui['statut'] == '0minirezo';
 }
 
 // supprimer
 function autoriser_pdformpatron_supprimer_dist($faire, $type, $id, $qui, $opt) {
 	return $qui['statut'] == '0minirezo';
 }
 
 
 // associer (lier / delier)
 function autoriser_associerpdformpatron_dist($faire, $type, $id, $qui, $opt) {
	 return $qui['statut'] == '0minirezo'; 
 }
 
 
 // -----------------
 // Objet pdform_cellules
 
 // bouton de menu
 function autoriser_pdformcellules_menu_dist($faire, $type, $id, $qui, $opts){
	 return $qui['statut'] == '0minirezo' AND !$qui['restreint']; 
 } 

 // bouton de menu Outils rapides
function autoriser_pdformcellulecreer_menu_dist($faire, $type, $id, $qui, $opt) {
	 return $qui['statut'] == '0minirezo' AND !$qui['restreint']; 
 }

 // creer
 function autoriser_pdformcellule_creer_dist($faire, $type, $id, $qui, $opt) {
	 return $qui['statut'] == '0minirezo'; 
 }
 
 // voir les fiches completes
 function autoriser_pdformcellule_voir_dist($faire, $type, $id, $qui, $opt) {
 	return in_array($qui['statut'], array('0minirezo','1comite')); 
}
 
 // modifier
 function autoriser_pdformcellule_modifier_dist($faire, $type, $id, $qui, $opt) {
	 return $qui['statut'] == '0minirezo' AND !$qui['restreint']; 
 }
 
 // supprimer
 function autoriser_pdformcellule_supprimer_dist($faire, $type, $id, $qui, $opt) {
	 return $qui['statut'] == '0minirezo'; 
}

 // associer (lier / delier)
 function autoriser_associerpdformcellule_dist($faire, $type, $id, $qui, $opt) {
	 return $qui['statut'] == '0minirezo'; 
 }


 // -----------------
 // Objet pdform_images
 
 // bouton de menu
 function autoriser_pdformimages_menu_dist($faire, $type, $id, $qui, $opts){
	 return $qui['statut'] == '0minirezo' AND !$qui['restreint']; 
 } 
 
 // bouton de menu Outils rapides
function autoriser_pdformimagecreer_menu_dist($faire, $type, $id, $qui, $opt) {
	 return $qui['statut'] == '0minirezo' AND !$qui['restreint']; 
 }

 // creer
 function autoriser_pdformimage_creer_dist($faire, $type, $id, $qui, $opt) {
	 return $qui['statut'] == '0minirezo'; 
 }
 
 // voir les fiches completes
 function autoriser_pdformimage_voir_dist($faire, $type, $id, $qui, $opt) {
 	return in_array($qui['statut'], array('0minirezo','1comite')); 
}
 
 // modifier
 function autoriser_pdformimage_modifier_dist($faire, $type, $id, $qui, $opt) {
	 return $qui['statut'] == '0minirezo' AND !$qui['restreint']; 
 }
 
 // supprimer
 function autoriser_pdformimage_supprimer_dist($faire, $type, $id, $qui, $opt) {
	 return $qui['statut'] == '0minirezo'; 
 }

 // associer (lier / delier)
 function autoriser_associerpdformimage_dist($faire, $type, $id, $qui, $opt) {
	 return $qui['statut'] == '0minirezo'; 
 }


 // -----------------
 // l'autorisation spéciale pour le peuplement de la base de données
 // #AUTORISER{peuplement,pdform_patron} 
 // cf : /pdfrom/prive/squelettes/navigation/pdform_patrons.html
 
 function autoriser_pdformpatron_peuplement_dist($faire, $type, $id, $qui, $opt) {
	 return $qui['statut'] == '0minirezo' AND !$qui['restreint']; 
 }
 
 // -----------------
 // l'autorisation spéciale pour une visualisation rapide du formulaire dans la fiche de l'objet patron
 // #AUTORISER{production,pdform_patron} 
 // cf : /pdform/prive/objets/contenu/pdform_patron.html
 
 function autoriser_pdformpatron_visualisation_dist($faire, $type, $id, $qui, $opt) {
	 return $qui['statut'] == '0minirezo'; 
 }

/**
 * Autoriser à télécharger le cache
 *
 * Le plugin Cache Factory (préfix ezcache) ne définit pas l'autorisation `cache`
 * appelée dans l'action `telecharger_cache`. 
 * Si cette autorisation n'a pas été définie par ailleur, il convient de le faire
 * pour permettre à tous les utilisateurs identifiés de télécharger
 * les documents.
 */
if (!function_exists('autoriser_cache_dist')){
	function autoriser_cache_dist($faire, $type, $id, $qui, $opt) {
		return in_array($qui['statut'], ['0minirezo', '1comite', '6forum']); 
	}
} 

// -----------------
 // l'autorisation spéciale pour la suppression du cache (reset du cache)
 // #AUTORISER{purger,pdform} 
 // cf : /pdform/action/pdform_purger.php
 
function autoriser_pdform_purger_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo','1comite')); 
}
