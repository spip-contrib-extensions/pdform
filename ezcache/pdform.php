<?php
 /**
  * Ce fichier contient la configuration des caches de PDForm basés sur l'API de Cache Factory.
  * Cache Factory (prefix:ezcache) est un plugin nécessaire au présent plugin et qui permet de gérer des fichiers caches.
  */
 if (!defined('_ECRIRE_INC_VERSION')) {
 	return;
 }
 
 
 /**
  * Renvoie la configuration spécifique des caches de PDForm.
  *
  * La fonction pdform_creer vous permet d'utiliser d'autres définitions de cache
  * et d'avoir d'autres normalisations de noms de fichier ou d'autres lieux de stockage.
  *
  * @param string $plugin
  *        Identifiant qui permet de distinguer le module appelant qui peut-être un plugin ou
  *        un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
  *
  * @return array
  *        Tableau de la configuration des caches du plugin PDForm.
  */
function pdform_cache_configurer($plugin) {
 
	// Initialisation du tableau de configuration avec les valeurs par défaut du plugin Cache Factory.
	// les fichiers seront placés et nommés de la façon suivante
	// _DIR_TMP/pdform/n°du patron (et éléments nourriciers).pdf

	return array(
		'pdf' => array(
			'racine'          => '_DIR_TMP', 
			'sous_dossier'    => false,
			'nom_obligatoire' => array('id_pdform_patron'), // Composants obligatoires ordonnés de gauche à droite, ici le patron seulement.
			'nom_facultatif'  => array('objets_nourriciers'), // Composants facultatifs, les objets nourissants de leurs données le patron
			'extension'       => '.pdf', // Extension du fichier cache, ici les fichiers .pdf produits.
			'securisation'    => false,
			'serialisation'   => false,
			'separateur'      => '-', // Caractère de séparation des composants '-' ou '' si un seul composant est utilisé
			'conservation'    => 0 // cache permanent
		)
	);
}