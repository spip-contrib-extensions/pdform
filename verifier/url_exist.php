<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Vérification de l'existance d'une URL
 *
 * @param string $valeur
 *   La valeur à vérifier.
 * @param array $options
 *   mode : strict, cool
 * @return string
 *   Retourne une chaine vide si c'est valide, sinon une chaine expliquant l'erreur.
 */

function verifier_url_exist_dist($valeur, $options = array()) {

	$regex = '#^(https?)\:\/\/.*$# i';


	if (!preg_match($regex, $valeur)) {
		if (!$options['mode'] or $options['mode'] == 'strict')
			// mode ’strict’ : on signale que ce n'est pas une url
			return _T('pdform:erreur_url_a_tester', array('url' => echapper_tags($valeur),'regex' => echapper_tags($regex)));
		else {
			// mode ’cool’ : pas besoin de tester si ce n'est pas une URL
			return '';
		}
	}
	
	// est-ce qu'on peut l'ouvrir ?
	$f = @fopen($valeur,"r");
	if ($f)
	{
		fclose($f);
		return ''; // oui
	}
	// non
	else return _T('verifier:erreur_url_exist', array('url' => echapper_tags($valeur),'regex' => echapper_tags($regex)));

}
