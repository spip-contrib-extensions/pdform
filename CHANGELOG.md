# Changelog

Changelog de PDForm
- Tenir un changelog : https://www.spip.net/fr_article6825.html
- Ecrire un commit : https://www.spip.net/fr_article6824.html

## [Unreleased]

### Added
- Commit ba2f8bc0 : permettre de créer un PDF uniquement avec le flot.

### Fixed
- Commit ba2f8bc0 : corriger dans le flot :
  o la prise en compte des images.
  o la prise en compte des formes.

### Changed
- Commit 929bb039 : Mise en conformité des fichiers de langue. Adopter la syntaxe valide à partir de SPIP 4.1 ce qui facilitera la migration en SPIP 5.
- remplacement des boucles POUR obsolètes par des boucles DATA. Le boucle POUR ne seront plus utilisable avec SPIP 5.

## [2.3.0] 2024-07-05

### Build
- Commit d4d22d39 Compatibilité SPIP 4.*
  
## [2.2.5] 2024-04-13

### Fixed
- #19 Cellule - permettre la prise en compte de la définition de la couleur en RGB ou niveau de gris. SetDrawColor dans la méthode PdformCell. 

### Refactor
- sécurisation de la définition de l'autorisation de téléchargement du cache.

## [2.2.4] 2023-12-30

### Removed
- #10 Cadrage - Suppression de squelettes inutiles pour les rôles et suppression de la référence à un rôle dans la déclaration de la table

### Added
- #8 Cellule - Signaler les tables nourricières et les éventuels conflits avec la liste des tables de la configuration.
- #13 Cadrage - Permettre d'avoir le chemin d'une image en utilisant sa référence (image1234). Ajout d'une constante pour énumérer les formats acceptés par la librairie FPDF _FPDF_FORMATS_IMG

### Fixed
- #15 Cadrage - Permettre les liens sur les images.
- #17 Cadrage - Permettre la création et l'association à la volée d'un cadrage sur un patron.

## [2.2.3] 2023-12-26

### Fixed
- #6 La liste des Cadrages gènère une erreur. Retirer l'appel à un champ inexistant (#TABLE_NOURRICIERE)