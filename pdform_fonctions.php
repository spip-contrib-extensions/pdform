<?php
 /**
  * Fonctions utiles au plugin
  *
  * @plugin     Pdform
  * @copyright  2021
  * @author     Vincent CALLIES
  * @licence    GNU/GPL
  * @package    SPIP\Pdform\Fonctions
  */
 
 if (!defined('_ECRIRE_INC_VERSION')) return;
 
 