<?php
/**
 *
 * @plugin     Pdform
 * @copyright  2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Pdform\Api
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * conversion d'une valeurs hexa en rgb et l'inverse
 *
 * link: https://www.php.net/manual/en/function.hexdec.php
 * @param string 
 * @return string
 */

function inc_pdform_couleurs_dist($c = '') : string {
   if(!$c) return false; 
   $c = trim($c); 
   $out = false; 
  if(preg_match("/^[0-9ABCDEFabcdef\#]+$/i", $c)){ 
      $c = str_replace('#','', $c); 
      $l = strlen($c) == 3 ? 1 : (strlen($c) == 6 ? 2 : false); 

      if($l){ 
         unset($out); 
         $out['red'] = hexdec(substr($c, 0,1*$l)); 
         $out['green'] = hexdec(substr($c, 1*$l,1*$l)); 
         $out['blue'] = hexdec(substr($c, 2*$l,1*$l)); 
		 $out = implode(',', $out);
      }else $out = false; 
              
   }elseif (preg_match("/^[0-9]+(,| |.)+[0-9]+(,| |.)+[0-9]+$/i", $c)){ 
      $spr = str_replace(array(',',' ','.'), ':', $c); 
      $e = explode(":", $spr); 
      if(count($e) != 3) return false; 
         $out = '#'; 
         for($i = 0; $i<3; $i++) 
            $e[$i] = dechex(($e[$i] <= 0)?0:(($e[$i] >= 255)?255:$e[$i])); 
              
         for($i = 0; $i<3; $i++) 
            $out .= ((strlen($e[$i]) < 2)?'0':'').$e[$i]; 
                  
         $out = strtoupper($out); 
   }else $out = false; 
          
   return $out; 
}
