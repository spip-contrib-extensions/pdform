<?php
/**
 * Création d'un PDF
 *
 * Utilise FPDF qui est une classe PHP qui permet de générer des fichiers PDF
 * en pur PHP, c'est-à-dire sans utiliser la librairie PDFlib. Le F de FPDF signifie Free.
 * Olivier Plathey, l'auteur de FPDF, est un Français.
 *
 * La librairie est utilisée au travers d'objets SPIP. Ces objets apportent l'information
 * nécessaire à la librairie sur le placement des images et du textes.
 * Il n'y a pas d'interprétation HTML des données SPIP avant d'être prises en compte par la librairie.
 * Cela évite une perte de la qualité de l'information, mais présente de la rigidité.
 *
 * @plugin     Pdform
 * @copyright  2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Pdform\Api
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction pour créer le PDF
 *
 * affichage du pdform_patron avec les
 * - pdform_cellules associées 
 *   définition d'une zone rectangulaire d'affichage textuel.
 * - pdform_images associées
 *   définition des images
 * - flot envoyé, sous deux formes, 
 *   des arguments clés valeurs pour des substitutions dans le texte des cellules
 *   des descriptions par page de texte à imprimer en flot
 *
 * @use pdform_balisage_dynamique() pour les cellules
 * @use pdform_chemin_dynamique() pour les images
 * @use pdform_verifier_ids() pour normaliser les objets nourriciers
 *
 * @param  integer     $id_pdform_patron
 *         identifiant unique du patron
 * @param  array       $ids
 *         identifiants uniques des objets (dont les données "habilleront" le patron)
 *         exemple : [auteur|1],[adresse|1],[numero|1]
 * @param  array       $flot
 *         arguments optionnels précisant "la coupe" du PDF
 *         $flot['options']['fichier_cache'] indique le chemin et nom du ficher PDF a créer
 *         $flot['textes'] indique les textes à ajouter
 *         $flot['images'] indique les images à ajouter
 *         $flot['formes'] indique les formes à ajouter
 *         par défaut, _DIR_TMP . 'doc.pdf';
 * @return array                      
 */
function pdform_creer(int $id_pdform_patron, array $ids, ?array $flot=[]) : array{

	# vérifications des arguments transmis

	// Le patron
	if ($id_pdform_patron > 0){
		if ($pdform_patron = sql_fetsel('*', 'spip_pdform_patrons', ["id_pdform_patron=$id_pdform_patron"])){
			spip_log( "build: pdform_patron n°$id_pdform_patron", 'pdform.' . _LOG_DEBUG);
		} else {
			return [
				'editable' => true,
				'message_erreur' => _T('pdform:notice_patron', ['id' => $id_pdform_patron]),
			];
		}
	}

	// Les objets
	if (!empty($ids)){
		if ($erreur = pdform_verifier_ids($ids)){
			return [
				'editable' => true,
				'message_erreur' => $erreur,
			];
		}
	}

	// Le flot
	if ($erreur = pdform_verifier_flot($flot)){
		return [
			'editable' => true,
			'message_erreur' => $erreur,
		];
	}
	if (!isset($pdform_patron)) {
		// sans patron, on se fie au flot
		// -- déterminer le nombre de page
		if (isset($flot['textes']) && $pages = array_keys($flot['textes'])){
			$pdform_patron['pages'] = max($pages);
		}	
		if (isset($flot['images']) && $pages = array_keys($flot['images'])){
			if (isset($pdform_patron['pages']) && max($pages) > $pdform_patron['pages']) {
				$pdform_patron['pages'] = max($pages);
			}
		}
		// -- sans texte ou image, indiquer une erreur
		if (!isset($pdform_patron['pages'])){
			return [
				'editable' => true,
				'message_erreur' => _T('pdform:notice_flot_et_patron'),
			];
		}
	}
	# appel de la librairie qui permet la création d'une class fpdf

	if (!include_spip(_DIR_FPDF_LIB.'fpdf') or !include_spip('inc/pdform_class')) {
		return ['message_erreur' => _T('pdform:message_erreur_class_indisponibles')];
	}
	$pdf = new PDFORM(); // Instanciation de la classe dérivée

	$pdf->AliasNbPages(); // Valeur par défaut : {nb}.

	# la police par defaut

	$pdf->SetFontdefaut([
			'family' => isset($pdform_patron['police']) ? $pdform_patron['police']:'helvetica',
			'style' => isset($pdform_patron['style']) ? $pdform_patron['style']:'',
			'size' => isset($pdform_patron['taille']) ? (float) $pdform_patron['taille']:0,
			'drawcolor' => isset($pdform_patron['couleur_trace']) ? $pdform_patron['couleur_trace']:'',
			'fillcolor' => isset($pdform_patron['couleur_rempli']) ? $pdform_patron['couleur_rempli']:'',
			'textcolor' => isset($pdform_patron['couleur_texte']) ?$pdform_patron['couleur_texte']:'',
		]
	);
	
	$pdf->Fontdefaut();

	# l'entête

	if (isset($pdform_patron['entete_texte'])) {
		$pdf->SetEntete( array(
			'family' => $pdform_patron['entete_police'],  // SetFont
			'style' => $pdform_patron['entete_style'], 
			'size' => $pdform_patron['entete_taille'],
			'drawcolor' => $pdform_patron['entete_couleur_trace'], // SetDrawColor
			'fillcolor' => $pdform_patron['entete_couleur_rempli'], // SetFillColor
			'textcolor' => $pdform_patron['entete_couleur_texte'], // SetTextColor est entete_couleur_texte
			'x' => $pdform_patron['entete_x'],  // SetXY
			'y' => $pdform_patron['entete_y'], 
			'w' => $pdform_patron['entete_largeur'],  // Cell
			'h' => $pdform_patron['entete_hauteur'], 
			'txt' => $pdform_patron['entete_texte'], 
			'border' => $pdform_patron['entete_bordure'],
			'ln' => $pdform_patron['entete_apres'],
			'align' => $pdform_patron['entete_align'],
			'fill' => $pdform_patron['entete_plein'],
			'link' => $pdform_patron['entete_lien'],
		) );
	}

	# pied de page

	if (isset($pdform_patron['pied_texte'])) {
		$pdf->SetPied( array(
			'family' => $pdform_patron['pied_police'], // SetFont
			'style' => $pdform_patron['pied_style'], 
			'size' => $pdform_patron['pied_taille'],
			'drawcolor' => $pdform_patron['pied_couleur_trace'], // SetDrawColor
			'fillcolor' => $pdform_patron['pied_couleur_rempli'], // SetFillColor
			'textcolor' => $pdform_patron['pied_couleur_texte'], // SetTextColor
			'x' => $pdform_patron['pied_x'], // SetXY
			'y' => $pdform_patron['pied_y'], 
			'w' => $pdform_patron['pied_largeur'], // Cell
			'h' => $pdform_patron['pied_hauteur'], 
			'txt' => $pdform_patron['pied_texte'], 
			'border' => $pdform_patron['pied_bordure'],
			'ln' => $pdform_patron['pied_apres'],
			'align' => $pdform_patron['pied_align'],
			'fill' => $pdform_patron['pied_plein'],
			'link' => $pdform_patron['pied_lien'],
		) );
	}

	include_spip('inc/filtres'); // fonctions pour les objets (id_table_objet et lister_tables_objets_sql)

	# création une à une des pages du PDF

	if (!isset($pdform_patron['pages'])) {
		$pdform_patron['pages'] = 1;
	}

	for ($page = 1; $page <= $pdform_patron['pages']; $page++) {

		$pdf->AddPage();
		spip_log("AddPage $page ", 'pdform.' . _LOG_DEBUG);


		# Les images du PDF
	
		$images = [];
	
		# images - pdform_images (les cadrages des images)

		if ($id_pdform_patron){
			if ($liens = sql_allfetsel('id_pdform_image', 'spip_pdform_images_liens', array('id_objet=' . intval($id_pdform_patron), 'objet="pdform_patron"', 'page='.$page))) {
				foreach ($liens as $cle => $image) { // pour chaque pdform_images associé au patron, on charge ses données
					if ($fond = sql_fetsel('*', 'spip_pdform_images', array('id_pdform_image=' . intval($image['id_pdform_image'])))) { 
						// permettre que le chemin soit indiqué par le flot
						if (isset($flot['arguments'])){
							$fond['imgfile'] = pdform_chemin_argument($fond['imgfile'], $flot['arguments']);
						}
						// permettre l'utilisation des ids dans le nom du chemin
						if ($fond['imgfile'] = pdform_chemin_dynamique($fond['imgfile'], $ids)){ 
							$images[] = $fond; // le chemin est validé
						}
					}
				}
			}
		}

		# images - récupérer celles du flots
		if (isset($flot['images'][$page])) {
			foreach ($flot['images'][$page] as $cle => $valeurs) {
				if (isset($valeurs['imgfile']) and is_string($valeurs['imgfile'])
					and isset($valeurs['imgx'])
					and isset($valeurs['imgy'])
				) {
					// Si la valeur de la hauteur ou de la largeur n'est pas indiquée,
					// elle est calculée automatiquement par la librairie
					$valeurs['imgw'] = ($valeurs['imgw'] ?? 0); 
					$valeurs['imgh'] = ($valeurs['imgh'] ?? 0); 
					// Si le type de l'image n'est pas indiqué, il est déduit de l'extension
					$valeurs['imgtype'] = ($valeurs['imgtype'] ?? ''); 
					// Le lien n'est pas obligatoire
					$valeurs['imglink'] = ($valeurs['imglink'] ?? ''); 
					$valeurs['imgfile'] = pdform_chemin_dynamique($valeurs['imgfile'], '');
					$images[] = $valeurs;
				}
			}
		}

		# images - affichage

		// Envoyer aux plugins pour qu'ils complètent (ou altèrent) la liste des images
		$images = pipeline('images_pdform', $images);
	 
		if ($images) { // affichage des images cadrées
			spip_log("pdform_creer. page = {$page} images. " . print_r($images, true), 'pdform.' . _LOG_DEBUG);
			foreach ($images as $cle => $fond) 
			{
				if ($fond['imgfile']) 
				{
					$pdf->Image($fond['imgfile'], $fond['imgx'], $fond['imgy'], $fond['imgw'],$fond['imgh'], $fond['imgtype'], $fond['imglink']);
				}
			}
		}

		# Les cellules du PDF

		$cellules = [];

		# cellules - pdform_cellules (les zones rectangulaires d'affichage de texte)

		if ($id_pdform_patron){
			if ($liens = sql_allfetsel('id_pdform_cellule', 'spip_pdform_cellules_liens', array('id_objet=' . intval($id_pdform_patron), 'objet="pdform_patron"', 'page='.$page))) {
				foreach ($liens as $cle => $cellule) { // pour chaque pdform_cellule associé au patron, on charge ses données
					# les champs de la cellule associée
					if ($champs = sql_fetsel('*', 'spip_pdform_cellules', array('id_pdform_cellule=' . intval($cellule['id_pdform_cellule'])))) {
						$table_objet = $champs['table_nourriciere'];
						$id_table_objet = id_table_objet($table_objet);
						$objet = objet_type($table_objet);
						# le champ txt appelle les valeurs de l'objet nourricier
						$texte = $champs['texte'];
						if ($champs['texte'] and $table_objet and $id_table_objet and $ids and isset($ids[$objet]) and $ids[$objet]) {
							# le txt
							$regex = '/@([a-z_0-9]+)@/m';
							preg_match_all($regex, $champs['texte'], $matches);
							if ($matches[1]) {
								// vérifier que les champs_demandés existent
								$champs_acceptables = lister_tables_objets_sql($champs['table_nourriciere']);
								$champs_acceptables = array_keys($champs_acceptables['field']);
								foreach ($matches[1] as $cle => $valeur) {
									if (!in_array($valeur,$champs_acceptables)) {
										spip_log( "page = {$page} " . $matches[0][$cle] . " ≠ ’field’ " . $champs['table_nourriciere'], 'pdform.' . _LOG_AVERTISSEMENT);
										unset ($matches[1][$cle]);
										unset ($matches[0][$cle]);
									}
								}
								if ($matches[1]
									and $ensemble = sql_fetsel($matches[1], $champs['table_nourriciere'], $id_table_objet . '=' . $ids[$objet])) {
									pdform_balisage_dynamique($ensemble, $champs['texte']);
									# substituer les valeurs aux endroits demandés (les @ @)
									$champs['texte'] = str_replace ($matches[0], $ensemble,$champs['texte']);
								}
							}
						}
						$cellules[] = $champs;
					}
				}
			}
		}

		# cellules - affichage

		// Envoyer aux plugins pour qu'ils complètent (ou altèrent) la liste des cellules
		$cellules = pipeline('cellules_pdform', $cellules);

		/**
		 * Analyse des données d'une cellule.
		 *
		 * La cellule est coupée en lignes à partir des sauts de lignes indiqués.
		 * Les balises sont analysées et remplacées par leurs valeurs ligne par ligne
		 * (ce qui permet la suppression d'une ligne sur la valeur est inexistante
		 * et que cela est demandé par le balisage dynamique - pipe).
		 */
		if ($cellules) { // affichage des cellules
			spip_log("pdform_visualiser_dist. page = {$page} cellules. " . print_r($cellules,true), 'pdform.' . _LOG_DEBUG);
			foreach ($cellules as $key => $champs) {
				$texte = explode("\n", $champs['texte']);
				$pdf->SetXY($champs['x'], $champs['y']);
				foreach ($texte as $txt) {
					$pdform_cell = [
						'family' => (string) $champs['police'],
						'style' => (string) $champs['style'], 
						'size' => (float) $champs['taille'], 
						'drawcolor' => $champs['couleur_trace'], 
						'fillcolor' => $champs['couleur_rempli'], 
						'textcolor' => $champs['couleur_texte'], 
						'x' => $champs['x'],  
						'w' => $champs['largeur'], 
						'h' => $champs['hauteur'],  
						'txt' => $txt,  
						'border' => $champs['bordure'],  
						'ln' => $champs['apres'],  
						'align' => $champs['align'],  
						'fill' => $champs['plein'],  
						'link' => $champs['lien'], 
					];
					// rendre les balises dynamiques
					pdform_balisage_dynamique($flot['arguments'], $pdform_cell['txt']);
					// procéder au remplacement des balises par leurs valeurs
					if (isset($flot['arguments']) and isset($pdform_cell['txt'])){
						pdform_balisage_remplacement($flot['arguments'], $pdform_cell['txt']);
					}
					// affichage
					$pdf->PdformCell($pdform_cell);
				}
				# retour à la normale
				$pdf->Fontdefaut();
			}
		}

		# formes en mode flot
		if (isset($flot['formes'][$page])) {
			foreach ($flot['formes'][$page] as $type => $forme) {
				if (isset($forme['SetLineWidth'])){
					$pdf->SetLineWidth($forme['SetLineWidth']);
				}
				// Fixe la couleur pour toutes les opérations de tracé 
				// (lignes, rectangles et contours de cellules).
				if (isset($forme['SetDrawColor'])){
					if ( isset($forme['SetDrawColor'][1]) and isset($forme['SetDrawColor'][2]) ){
						$pdf->SetDrawColor(
							$forme['SetDrawColor'][0],
							$forme['SetDrawColor'][1],
							$forme['SetDrawColor'][2]
						);
					} else {
						$pdf->SetDrawColor($forme['SetDrawColor'][0]);
					}
				}
				if (isset($forme['SetFillColor'])){
					if ( isset($forme['SetFillColor'][1]) and isset($forme['SetFillColor'][2]) ){
						$pdf->SetFillColor(
							$forme['SetFillColor'][0],
							$forme['SetFillColor'][1],
							$forme['SetFillColor'][2]
						);
					} else {
						$pdf->SetFillColor($forme['SetFillColor'][0]);
					}
				}
				if (isset($forme['type'])) {
					if ($forme['type'] == 'Rect'){
						$pdf->Rect($forme['x'], $forme['y'], $forme['w'], $forme['h'] , $forme['style']);
					}
					if ($forme['type'] == 'RoundedRect'){
							$pdf->RoundedRect($forme['x'], $forme['y'], $forme['w'], $forme['h'], $forme['r'], $forme['style']);
					}
					if ($forme['type'] == 'Circle'){
						$pdf->Circle($forme['x'], $forme['y'], $forme['r'], $forme['style']);
					}
					if ($forme['type'] == 'Ellipse'){
						$pdf->Ellipse($forme['x'], $forme['y'], $forme['rx'], $forme['ry'], $forme['style']);
					}
					if ($forme['type'] == 'Line'){
						$pdf->Line($forme['x1'], $forme['y1'], $forme['x2'], $forme['y2']);
					}
				}
				$pdf->SetDrawColor(0,0,0);
				$pdf->SetFillColor(0,0,0);
				$pdf->SetLineWidth(0.5);
			}
		}

		# le texte en mode flot
		if (isset($flot['textes'][$page])) {
			spip_log("pdform_visualiser_dist. flot page {$page} " . print_r($flot['textes'][$page],true), 'pdform.' . _LOG_DEBUG);
			foreach ($flot['textes'][$page] as $cle => $texte) {
				// changement de police...
				if ( isset($texte['family']) and !empty($texte['family']) ){
					// ... et changement de style
					if ( isset($texte['style']) and !empty($texte['style']) ){
						$pdf->SetFont($texte['family'], $texte['style'], $texte['size']);
					} else {
						$pdf->SetFont($texte['family']);
					}
				}
				// changement de taille 
				if ( isset($texte['size']) and !empty($texte['size']) ){
					$pdf->SetFontSize($texte['size']);
				}
				// Changement de couleur
				if ( isset($texte['textcolor']) and !empty($texte['textcolor']) ){
					if (is_integer($texte['textcolor'])){
						$pdf->SetTextColor($texte['textcolor']);
					} elseif (isset($texte['textcolor']['r']) && !isset($texte['textcolor']['g']) ) {
						$pdf->SetTextColor($texte['textcolor']['r']);
					} elseif (isset($texte['textcolor']['r']) && isset($texte['textcolor']['g']) && isset($texte['textcolor']['b'])){
						$pdf->SetTextColor($texte['textcolor']['r'],$texte['textcolor']['g'],$texte['textcolor']['b']);
					}
				}

				// permettre un flot en colonne en déplaçant la marge
				if ($texte['x'] <> 10) {
					$pdf->SetLeftMargin($texte['x']);
				}
				$pdf->SetXY($texte['x'], $texte['y']);
				// voir comment appeler la fonction uft_intelligent de la class
				$pdf->Write($texte['h'], mb_convert_encoding($texte['txt'], 'windows-1252'), $texte['link']);
				//$pdf->Write($texte['h'], utf_decode_intelligent($texte['txt']), $texte['link']);
				// remettre la marge d'origine
				if ($texte['x'] <> 10) {
					$pdf->SetLeftMargin(10);
				}
			}
		}

	} // fin de la composition des pages du PDF (foreach)
	
	# on enregistre le fichier cache
	$pdf->Output($flot['options']['fichier_cache'], 'F'); 
	// on retourne des éléments pour géré le pdf créer. 
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$url = $securiser_action('telecharger_cache', $flot['options']['fichier_cache']);
		return array(
		'editable' => (isset($options['editable']) and $options['editable']) ? $options['editable'] : false,
		// Le message_ok donne un lien direct sur le fichier (qui demande à être visible)
		'message_ok' =>_T('pdform:message_creation_ok', ['url_pdf' => $url]),
		'url_pdf' => $flot['options']['fichier_cache'],
		'nom_pdf' => basename($flot['options']['fichier_cache']),
	);

}  

/**
 * Fonction privée permettant de rendre un peu dynamique (id) et souple (path)
 * la définition du chemin d'une image.
 *
 * @param  string         $chemin (adresse de la variable, qui sera donc changée par la fonction)
 * @param  array          $ids identifiants des objets nourriciers
 * @return string|boolean $chemin normalisé ou FALSE si échec                     
 */
function pdform_chemin_dynamique(string $chemin, array $ids){
	$normaliser_chemin = false;
	if ($chemin) { // il faut au mimimum une image
		// Le nom de l'image peut être personnalisée par son id
		// exemple : pdform/auteurs/image_auteur_@id_auteur@.jpg
		if ($ids and preg_match('#@id_([a-z_]*)@#', $chemin, $matches)) {
			if ($id = $ids[$matches[1]]){
				$normaliser_chemin = str_replace('@id_'.$matches[1].'@',$id,$chemin);
				$chemin = $normaliser_chemin;
			}
		}
		// Le nom de l'image peut être à chercher dans un document
		// (On utilise le mot image plutôt que doc.
		// image.html est le modèle introduit par SPIP 4.0).
		// exemple : image1757
		if (preg_match('#^image([0-9]+)$#', $chemin, $matches)) {
			if ($id_document = $matches[1] and $id_document > 0) {
				if ($infos = sql_fetsel('extension,fichier', 'spip_documents', 'id_document=' . $id_document)){
					if (in_array($infos['extension'], _FPDF_FORMATS_IMG)){
						$chemin = $infos['fichier'];
					} else {
						spip_log("La création d’un chemin dynamique sur la base de la référence à l’image n°{$id_document} n'est pas possible : l’extension {$infos['extension']} n’est pas supportée par la librairie FPDF (voir la constante dans le fichier pdform_options.php _FPDF_FORMATS_IMG).", 'pdform.' . _LOG_DEBUG);
						return FALSE;
					}
				} else {
					spip_log("La création d’un chemin dynamique sur la base de la référence à l’image n°{$id_document} n'est pas possible : la requete au serveur de base de données ne retourne aucune information.", 'pdform.' . _LOG_DEBUG);
					return FALSE;
				}
			} else {
				return FALSE;
			}
		}
		// le lien vers le fichier sera probablement dans le dossier IMG
		if (file_exists(_DIR_IMG . $chemin)) {
			$normaliser_chemin = _DIR_IMG . $chemin;
		} elseif ($f = find_in_path($chemin)) {
		// le lien peut être dans le path de SPIP
			$normaliser_chemin = $f;
		} else {
		// le lien peut être une URL
			$verifier = charger_fonction('verifier', 'inc/'); // la fonction centrale du plugin Vérifier
			$e = $verifier($chemin, 'url', array('type_protocole' => 'web', 'mode' => 'complet'));// qu'on utilise
			if ($e){
				return false;
			} else {
				$normaliser_chemin = $chemin;
			}
		}
	}
	return $normaliser_chemin;
}

/**
 * Fonction privée permettant de proposer le chemin d'une image en argument dans le flot
 *
 * @param  string         $chemin proposé par le pdform_image
 * @param  array          $arguments pouvant se substituer au chemin
 * @return string         $chemin normalisé ou $chemin initial si échec                     
 */
function pdform_chemin_argument(string $chemin, array $arguments){
	if (preg_match('#@([a-z0-9_]*)@#', $chemin, $matches)) {
		if (isset($arguments[$matches[1]]) and $substitution = trim($arguments[$matches[1]]) and preg_match('/^[^*?"<>|:]*$/',$substitution) ){
			$chemin = str_replace('@'.$matches[1].'@',$substitution,$chemin);
		}
	}
	return $chemin;
}

/**
 * Fonction privée permettant de rendre un peu dynamique le balisage
 *
 * @param  array           $valeurs indication des balises en un tableau clé (nom de la balise) valeur (de remplacement)
 * @param  string          $matrice texte dans lequel se trouve les clés à remplacer par les valeurs
 * @return void            pas de valeur de retour : les arguments de la fonction sont changés                    
 */
function pdform_balisage_dynamique(&$valeurs, &$matrice){

	# opération : @se_substitue_a@>@cela@ si @se_substitue_a@ existe   exemple Vous êtes @multi_transport@>@voiture@

	$regex = '/@([a-z_0-9]+)@>@[a-z_0-9]+@/m';
	$ok = preg_match_all($regex, $matrice, $matches);
	if ($ok) {
		foreach ($matches[1] as $cle => $valeur) {
			if (isset ($valeurs[$valeur]) and $valeurs[$valeur]) { // la valeur existe
				$regex = '/(@[a-z_0-9]+@)(>@[a-z_0-9]+@)/m';
				$remplacement = '$1'; // on garde la référence et enlève l'autre
				spip_log($valeur . '(' . $valeurs[$valeur] . ") > " . $matches[0][$cle], 'pdform.' . _LOG_DEBUG);
				$matrice = preg_replace($regex, $remplacement, $matrice, 1);
			} else {
				$regex = '/(@[a-z_0-9]+@>)(@[a-z_0-9]+@)/m';
				$remplacement = '$2'; // on fait l'inverse
				spip_log($valeur . '(' . $valeurs[$valeur] . ")  ≠ " . $matches[0][$cle], 'pdform.' . _LOG_DEBUG);
				$matrice = preg_replace($regex, $remplacement, $matrice, 1);
			}
		}
	}
	// cas spécifique des dates
	$regex = '/@([a-z_0-9]+)@\|affdate>@[a-z_0-9]+@/m'; 
	if (preg_match_all($regex, $matrice, $matches)) {
		foreach ($matches[1] as $cle => $valeur) {
			if ($valeurs[$valeur] and $valeurs[$valeur] != '0000-00-00 00:00:00') { // la valeur existe
				$regex = '/(@[a-z_0-9]+@\|affdate)(>@[a-z_0-9]+@)/m';
				$remplacement = '$1'; // on garde la référence et enlève l'autre
				spip_log($valeur . '(' . $valeurs[$valeur] . ") > " . $matches[0][$cle], 'pdform.' . _LOG_DEBUG);
				$matrice = preg_replace($regex, $remplacement, $matrice, 1);
			} else {
				$regex = '/(@[a-z_0-9]+@\|affdate>)(@[a-z_0-9]+@)/m';
				$remplacement = '$2'; // on fait l'inverse
				spip_log($valeur . '(' . $valeurs[$valeur] . ")  ≠ " . $matches[0][$cle], 'pdform.' . _LOG_DEBUG);
				$matrice = preg_replace($regex, $remplacement, $matrice, 1);
			}
		}
	}
	// substitution orthodoxe SPIP
	$regex = '/@([a-z_0-9]+)@sinon@[a-z_0-9]+@/m'; 
	if (preg_match_all($regex, $matrice, $matches)) {
		foreach ($matches[1] as $cle => $valeur) {
			if ($valeurs[$valeur] and $valeurs[$valeur] != '0') { // la valeur existe et n'est pas à zéro
				$regex = '/(@[a-z_0-9]+@)(sinon@[a-z_0-9]+@)/m';
				$remplacement = '$1'; // on garde la référence et enlève l'autre
				spip_log($valeur . '(' . $valeurs[$valeur] . ") sinon oui " . $matches[0][$cle], 'pdform.' . _LOG_DEBUG);
				$matrice = preg_replace($regex, $remplacement, $matrice, 1);
			} else {
				$regex = '/(@[a-z_0-9]+@sinon)(@[a-z_0-9]+@)/m';
				$remplacement = '$2'; // on fait l'inverse
				spip_log($valeur . '(' . $valeurs[$valeur] . ")  sinon niet " . $matches[0][$cle], 'pdform.' . _LOG_DEBUG);
				$matrice = preg_replace($regex, $remplacement, $matrice, 1);
			}
		}
	}
	
	# opération : @si_existe@<@se_substitue@  (et sinon n'affiche rien)

	$regex = '/@([a-z_0-9]+)@<@[a-z_0-9]+@/m';
	$ok = preg_match_all($regex, $matrice, $matches);
	if ($ok) {
		foreach ($matches[1] as $cle => $valeur) {
			if (isset($valeurs[$valeur]) and $valeurs[$valeur]) { // la valeur existe
				$regex = '/(@[a-z_0-9]+@<)(@[a-z_0-9]+@)/m';
				$remplacement = '$2'; // on la substitue à l'autre
				spip_log($valeur . '(' . $valeurs[$valeur] . ") < " . $matches[0][$cle], 'pdform.' . _LOG_DEBUG);
				$matrice = preg_replace($regex, $remplacement, $matrice, 1);
			} else {
				$regex = '/(@[a-z_0-9]+@<@[a-z_0-9]+@)/m';
				$remplacement = ''; // on ne met rien
				spip_log($valeur . '(' . $valeurs[$valeur] . ")  ≠ " . $matches[0][$cle], 'pdform.' . _LOG_DEBUG);
				$matrice = preg_replace($regex, $remplacement, $matrice, 1);
			}
		}
	}
/*	// cas spécifique des dates
	$regex = '/@([a-z_0-9]+)@\|affdate<@[a-z_0-9]+@/m'; 
	if (preg_match_all($regex, $matrice, $matches)) {
		foreach ($matches[1] as $cle => $valeur) {
			if ($valeurs[$valeur] and $valeurs[$valeur] != '0000-00-00 00:00:00') { // la valeur existe
				$regex = '/(@[a-z_0-9]+@\|affdate)(<@[a-z_0-9]+@)/m';
				$remplacement = '$1'; // on garde la référence et enlève l'autre
				spip_log($valeur . '(' . $valeurs[$valeur] . ") > " . $matches[0][$cle], 'pdform.' . _LOG_DEBUG);
				$matrice = preg_replace($regex, $remplacement, $matrice, 1);
			} else {
				$regex = '/(@[a-z_0-9]+@\|affdate<@[a-z_0-9]+@)/m';
				$remplacement = ''; // on ne met rien
				spip_log($valeur . '(' . $valeurs[$valeur] . ")  ≠ " . $matches[0][$cle], 'pdform.' . _LOG_DEBUG);
				$matrice = preg_replace($regex, $remplacement, $matrice, 1);
			}
		}
	}
*/	
	/**
	 * opération : |@supprimer_la_ligne_si_vide@   exemple @voie@
	 *                                                     |@complement@
	 *                                                     @code_postal@ @commune@
	 * On supprime toute la ligne avec ce qu'il y a avant ou après qui peut être du contexte. 
	 * Si plusieurs lignes, définir la cellule :
	 * - ’post-affichage’ à ’en dessous’
	 * - ’hauteur’ à 5 (ou selon la taille de la police)
	 * La ligne est totalement supprimée dont le retour (le texte remonte donc, s'est voulu)
	 */

	$regex = '/\|@([a-z_0-9]+)@/m';
	$ok = preg_match_all($regex, $matrice, $matches);
	if ($ok) {
		foreach ($matches[1] as $cle => $valeur) {
			if (isset($valeurs[$valeur]) and $valeurs[$valeur] and $valeurs[$valeur] != '0000-00-00 00:00:00') { // la valeur existe (et n'est pas un Timestamp vide)
				$regex = '/(\|)(@[a-z_0-9]+@)/m';
				$remplacement = '$2'; // on garde la référence et enlève le ’pipe’
				spip_log('La valeur "' . $valeurs[$valeur] . '" existe pour ' . $matches[0][$cle], 'pdform.' . _LOG_DEBUG);
				$matrice = preg_replace($regex, $remplacement, $matrice, 1);
			} else {
				// on supprime toute la ligne car la valeur n'existe pas
				$regex = '/(.*\|@[a-z_0-9]+@.*\n?)/';
				$remplacement = '';
				spip_log('La valeur n’existe pas pour ' . $matches[0][$cle], 'pdform.' . _LOG_DEBUG);
				$matrice = preg_replace($regex, $remplacement, $matrice, 1);
			}
		}
	}

	# opération : @date@|affdate{"formatPHP"}   exemple @maj@|affdate{"d/m/Y"} -> 31/12/2021

	$regex ='/@([a-z_0-9]+)@\|affdate{["|\']([^"|\']+)["|\']}/'; // @([a-z_0-9]+)@\|affdate{["|']([^"|']+)["|']}
	if (preg_match_all($regex, $matrice, $matches)) {
		foreach ($matches[1] as $cle => $valeur) {
			if (isset($valeurs[$valeur]) and $valeurs[$valeur]) { // la valeur existe. Elle pourrait être now ou + 1 day ou un timestamp
				$time = strtotime($valeurs[$valeur]);
				$conversion = date($matches[2][$cle],$time);
				spip_log('La valeur "' . $conversion . '" existe pour ' . $matches[0][$cle], 'pdform.' . _LOG_DEBUG);
				$matrice = preg_replace($regex, $conversion, $matrice, 1);
				
			}
		}
	}

	# opération : @date@|affdate   exemple @maj@|affdate  -> vendredi 31 décembre 2021

	$regex = '#@([a-z_0-9]+)@\|affdate($|\s|\.)#'; 
	if (preg_match_all($regex, $matrice, $matches)) 
	{
		foreach ($matches[1] as $cle => $valeur) 
		{      // la valeur existe mais est null -> on ne met rien.
			if (isset($valeurs[$valeur]) and $valeurs[$valeur] == '0000-00-00 00:00:00') {
				$valeurs[$valeur] = '';
				$conversion = '';
				$matrice = preg_replace($regex, $conversion."$2", $matrice, 1);
			}
			if (isset($valeurs[$valeur]) and strlen($valeurs[$valeur]))
			{ // la valeur existe. Elle pourrait être now ou + 1 day ou un timestamp
				include_spip('inc/filtres_dates');
				$conversion = nom_jour($valeurs[$valeur]) . ' ' . affdate($valeurs[$valeur]);
				$matrice = preg_replace($regex, $conversion."$2", $matrice, 1);
			} 
		}
	}

	# opération : @date@|affdate_heure   exemple @maj@|affdate_heure  -> vendredi 31 décembre 2021 à 12h00

	$regex = '#@([a-z_0-9]+)@\|affdate_heure($|\s|\.)#';
	if (preg_match_all($regex, $matrice, $matches)) 
	{
		foreach ($matches[1] as $cle => $valeur) 
		{
			if ($valeurs[$valeur]) 
			{ // la valeur existe. Elle pourrait être now ou + 1 day ou un timestamp
				include_spip('inc/filtres_dates');
				$conversion = nom_jour($valeurs[$valeur]) . ' ' . affdate($valeurs[$valeur]);
				$conversion .= ' ' . _T('pdform:date_a', array('h' => heures($valeurs[$valeur]), 'm' => minutes($valeurs[$valeur])));
				//$conversion .= ' ' . affdate_heure($valeurs[$valeur]);
				// on retire le ’min’
				//$conversion = substr($conversion, 0, -3);
				$matrice = preg_replace($regex, $conversion."$2", $matrice, 1);
			}
		}
	}
	
	# opération : @date@|affdate_debut_fin   exemple @maj@|affdate_debut_fin  -> Du jeudi 27 mai à 15h43 au vendredi 31 décembre 2021 à 00h00 où $valeurs['maj'] = '2021-05-27 15:43:00|2021-12-31 00:00:00';

	$regex = '#@([a-z_0-9]+)@\|affdate_debut_fin($|\s|\.)#';
	if (preg_match_all($regex, $matrice, $matches)) 
	{
		foreach ($matches[1] as $cle => $valeur) 
		{
			if ($valeurs[$valeur]) 
			{ // la valeur existe. Elle pourrait être now ou + 1 day ou un timestamp
				//on explose la valeur (il doit y avoir deux dates)
				if ($date = explode('|', $valeurs[$valeur])){
					include_spip('inc/filtres_dates');
					$conversion = affdate_debut_fin($date[0],$date[1],'oui','jourannee');
					$matrice = preg_replace($regex, $conversion."$2", $matrice, 1);
				}
			}
		}
	}

	return;
}

/**
 * Fonction privée précédant au remplacement du balisage par les valeurs correspondantes
 *
 * @param  array           $arguments indication des balises en un tableau clé (nom de la balise) valeur (de remplacement)
 * @param  string          $txt texte dans lequel se trouve les clés à remplacer par les valeurs
 * @return void            pas de valeur de retour : le texte transmis en argument est changé                    
 */
function pdform_balisage_remplacement($arguments,&$txt){
	foreach ($arguments as $cle => $valeur) {
		if ($valeur !== null){
			$txt = str_replace('@'.$cle.'@', $valeur, $txt);
		}
	}
}

/**
 * Fonction privée verifiant la validiter des arguments du flot
 *
 * @param  array   $flot 
 * @return string  chaine vide s'il n'y a pas d'erreur
 *                 message d'erreur
 */
function pdform_verifier_flot(&$flot){
	// -- les options
	if (!isset($flot['options']['fichier_cache'])) {
		// créer le nom du futur fichier pdf par défaut
		$flot['options']['fichier_cache'] = _DIR_TMP . 'doc.pdf';
	} elseif (!(string) $flot['options']['fichier_cache']) {
		return _T('pdform:notice_fichier_cache');
	} else {
		// le nom du futur fichier pdf est précisé
		// le chemin est-il exact ?
		if (!is_dir(dirname( $flot['options']['fichier_cache'] ))){
			return _T('pdform:notice_flot_dirname', ['file' => $flot['options']['fichier_cache']]);
		}
	}
	// -- les textes
	if (isset($flot['textes']) and !empty($flot['textes'])) {
		foreach ($flot['textes'] as $page => $cellules) {
			// La page
			if (!$page = (int) $page){
				return _T('pdform:notice_flot_page', ['page' => $page]);
			}
			// La cellule
			foreach ($cellules as $nom => $valeurs) {
				$err = [];
				if (isset($valeurs['x']) and !is_numeric($valeurs['x'])) {
					$err[] = 'x NOT numeric';
				}
				if (isset($valeurs['y']) and !is_numeric($valeurs['y'])) {
					$err[] = 'y NOT numeric';
				}
				if (isset($valeurs['h']) and !is_numeric($valeurs['h'])) {
					$err[] = 'h NOT numeric';
				}
				if ( !empty($err) ){
					return _T('pdform:notice_flot_cellule_xyh',['err' => print_r($err,true), 'page' => $page, 'cellule' => $nom]);
				}
				if (isset($valeurs['style']) and strlen($valeurs['style']) and !in_array($valeurs['style'], ['B','I','U'])){
						return _T('pdform:notice_flot_cellule_style',['err' => $valeurs['style'], 'page' => $page, 'cellule' => $nom]);
				}
				if (isset($valeurs['size'])){
					$size = (float) $valeurs['size'];
					if ($valeurs['size'] != $size){
						return _T('pdform:notice_flot_cellule_size', ['err' => 'size NOT float', 'page' => $page, 'cellule' => $nom]);
					}
				} 
			}
		}
	}
	return '';
}
/**
 * Fonction privée verifiant la validiter des arguments relatif aux objets nourriciers (ids)
 *
 * @param  array   $ids 
 * @return string  chaine vide s'il n'y a pas d'erreur
 *                 message d'erreur
 */
function pdform_verifier_ids(&$ids){
	include_spip('inc/filtres'); // fonctions pour les objets (id_table_objet et lister_tables_objets_sql)
	// ids peut être : vide | une chaine | un tableau. Deviendra vide | tableau vérifié
	if ($ids and is_string($ids)){ // $ids est une chaine, on la transforme en tableau
		$identifiants_nourriciers = array();
		$lignes = explode(',', $ids); // Les ids peuvent être séparés par des virgules
		foreach($lignes as $cle => $ligne) {
			list($id_table_objet, $id_objet) = explode('|', $ligne);
			if ($id_table_objet and intval($id_objet)) {
				$identifiants_nourriciers[trim($id_table_objet)] = intval($id_objet); // tableau : clé objet valeur id
			}
		}
		$ids = $identifiants_nourriciers;
		unset($identifiants_nourriciers,$id_table_objet,$id_objet,$lignes);
	}
	if ($ids and is_array($ids)){ // on vérifie la validité du tableau transmis
		$verifier = charger_fonction('verifier', 'inc/'); // On charge la fonction centrale du plugin Verifier
		foreach($ids as $cle => $valeur) {
			if (substr($cle,0,3) == 'id_') { // On transforme l'identifiant en objet si nécessaire
				$nllecle = substr($cle,3);
				$ids[$nllecle] = $valeur;
				unset($ids[$cle]);
				$cle = $nllecle;
			} 
			$erreur = $verifier($valeur, 'id_objet', array('objet' => $cle)); // On ’vérifie’ id | objet
			if ($erreur) { // La fonction retourne une chaîne vide si la vérification se passe bien. 
				return array("message_erreur" => $erreur);
			}
		}
	}
	return '';
}

/**
 * Vérifie si le cache est présent. 
 * Crée un lien pour visualiser le cache (et un bouton pour le supprimer), ou rien en cas d'absence.
 *
 * @param string $fichier_cache 
 * @param string $retour, page de retour si on supprime le cache
 *                        exemple : $url = generer_url_ecrire('pdform_patron', "id_pdform_patron=".$id_pdform_patron)
 *
 * @return array $fichier_cache sous différentes présentations dont un 'message_ok'
 */
function pdform_utiliser_cache($fichier_cache,$retour) {
	$retours ='';
	# est-ce utile de préparer ? (où le droit à la paresse du serveur)
	// le fichier existe, on l'utilise ! 
	if (is_file($fichier_cache)) {
		// permettre de purger le cache dans l'espace privé par une petite icone
		if (test_espace_prive() and autoriser('purger','pdform')){ 
			$securiser_action = charger_fonction('securiser_action', 'inc');
			$url = $securiser_action('pdform_purger', $fichier_cache, $retour);
			$img = 'supprimer-8.svg';
			$alt = _T('pdform:icone_supprimer_cache');
			$title =  _T('pdform:icone_supprimer_cache');
			$icone = "<a href='$url'>" . http_img_pack($img, $alt, '', $title) . '</a>';
			$editable = false;
		} else {  // dans l'espace public une petite icone indique discrètement que le fichier est en cache
			$img = 'cache-8.png';
			$alt = _T('pdform:icone_cache');
			$title =  _T('pdform:icone_cache');
			$icone = http_img_pack($img, $alt, '', $title) ;
			$editable = true;
		}
		// le fichier pdf étant dans le repertoire TMP pour y accéder il faut une action pour l'extraire.
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$url = $securiser_action('telecharger_cache', $fichier_cache);
		$retours = array(
			'editable' => $editable,
			'message_ok' => _T('pdform:message_creation_ok',array('url_pdf'=>$url)) . ' ' . $icone,
			'url_pdf' => $fichier_cache,
			'nom_pdf' => basename($fichier_cache),
		);
	}
	return $retours;
}

