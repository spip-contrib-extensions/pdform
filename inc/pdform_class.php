<?php
/**
 * Fichier proposant une class et diverses fonctions nécessaires à la création des PDF
 *
 * @plugin     Pdform
 * @copyright  2021
 * @author     Thrax
 * @licence    GNU/GPL
 * @package SPIP\Pdform\Inc
*/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip(_DIR_FPDF_LIB . 'fpdf');


	class PDFORM extends FPDF
	{

		private $_pied = array(),
				$_entete =  array(),
				$_fontdefaut = array();
	// auteur de la fonction Auteur : Maxime Delorme Licence : FPDF
    function RoundedRect($x, $y, $w, $h, $r, $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
        $xc = $x+$w-$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

        $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
        $xc = $x+$w-$r ;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
        $xc = $x+$r ;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }

	function Circle($x, $y, $r, $style='D')
		{
			$this->Ellipse($x,$y,$r,$r,$style);
		}

	function Ellipse($x, $y, $rx, $ry, $style='D')
		{
			if($style=='F')
				$op='f';
			elseif($style=='FD' || $style=='DF')
				$op='B';
			else
				$op='S';
			$lx=4/3*(M_SQRT2-1)*$rx;
			$ly=4/3*(M_SQRT2-1)*$ry;
			$k=$this->k;
			$h=$this->h;
			$this->_out(sprintf('%.2F %.2F m %.2F %.2F %.2F %.2F %.2F %.2F c',
				($x+$rx)*$k,($h-$y)*$k,
				($x+$rx)*$k,($h-($y-$ly))*$k,
				($x+$lx)*$k,($h-($y-$ry))*$k,
				$x*$k,($h-($y-$ry))*$k));
			$this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
				($x-$lx)*$k,($h-($y-$ry))*$k,
				($x-$rx)*$k,($h-($y-$ly))*$k,
				($x-$rx)*$k,($h-$y)*$k));
			$this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
				($x-$rx)*$k,($h-($y+$ly))*$k,
				($x-$lx)*$k,($h-($y+$ry))*$k,
				$x*$k,($h-($y+$ry))*$k));
			$this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c %s',
				($x+$lx)*$k,($h-($y+$ry))*$k,
				($x+$rx)*$k,($h-($y+$ly))*$k,
				($x+$rx)*$k,($h-$y)*$k,
				$op));
		}
		// Mutateur chargé de modifier l'attribut $_fontdefaut.
		public function setFontdefaut($fontdefaut)
		{
			if (!is_array($fontdefaut)) // S'il ne s'agit pas d'un tableau.
			{
				trigger_error('La définition de la Police par défaut doit être sous la forme ’array’', E_USER_WARNING);
				return;
			} 
			$this->_fontdefaut = $fontdefaut;
		}
		// Donner $_fontdefaut.
		public function getFontdefaut()
		{
			return $this->_fontdefaut;
		}
		// Fixer $_fontdefaut.
		public function Fontdefaut()
		{
			$f = $this->_fontdefaut;
			$this->SetFont($f['family'], $f['style'], $f['size']);
			if ($f['drawcolor']) 
			{
				$color = explode(',', $f['drawcolor']);
				if (is_array($color))
				{
					$this->SetDrawColor(intval($color[0]),intval($color[1]),intval($color[2])); // rouge vert bleu (rgb)
				} else 
				{
					$this->SetDrawColor(intval($color)); // nuance de gris
				}
			}
			if ($f['fillcolor']) 
			{
				$color = explode(',', $f['fillcolor']);
				if (is_array($color))
				{
					$this->SetFillColor(intval($color[0]),intval($color[1]),intval($color[2])); // rouge vert bleu (rgb)
				} else 
				{
					$this->SetFillColor(intval($color)); // nuance de gris
				}
			}
			if ($f['textcolor']) 
			{
				$color = explode(',', $f['textcolor']);
				if (is_array($color))
				{
					$this->SetTextColor(intval($color[0]),intval($color[1]),intval($color[2])); // rouge vert bleu (rgb)
				} else 
				{
					$this->SetTextColor(intval($color)); // nuance de gris
				}
			}
		}
		// Mutateur chargé de modifier l'attribut $_pied.
		public function setPied($pied)
		{
			if (!is_array($pied)) // S'il ne s'agit pas d'un tableau.
			{
				trigger_error('La définition du pied doit être sous la forme ’array’', E_USER_WARNING);
				return;
			} 
			$this->_pied = $pied;
		}
		
		public function setEntete($entete)
		{
			if (!is_array($entete)) // S'il ne s'agit pas d'un tableau.
			{
				trigger_error('La définition de l’entete doit être sous la forme ’array’', E_USER_WARNING);
				return;
			} 
			$this->_entete = $entete;
		}

		// En-tête
		function Header()
		{
			$e = $this->_entete;
			if (
				( isset($e['pages']) and in_array($this->PageNo(), calcul_designation_pages($e['pages'])) ) 
				or 
				!isset($e['pages'])
				)
			{
				if (isset($e['x']) and isset($e['y'])){
					$this->SetXY($e['x'], $e['y']);
				}
				$this->PdformCell($e); // affichage texte
				$this->Fontdefaut();
			}
		}
		
		// Pied de page
		function Footer()
		{
			$p = $this->_pied;
			if (!isset($p['pages']) or in_array($this->PageNo(), calcul_designation_pages($p['pages']) ))
			{ // pied de page si demandé
				if (isset($p['txt']))
				{
					$this->SetXY($p['x'],$p['y']);
					$p['txt'] = $p['txt'] . ' ' . $this->PageNo().'/{nb}';
					$this->PdformCell($p); // affichage texte
					$this->Fontdefaut();
				}
			}
		}

		// Cellule complète
		public function PdformCell(array $c)
		{
			if (isset($c['txt']))
			{
				# convertir de l'encodage actuel utilisé à windows-1252
				$c['txt'] = mb_convert_encoding($c['txt'], 'windows-1252');
				# La police
				if (strlen($c['family'])){
					if ($c['size'] > 0){
						$this->SetFont($c['family'], $c['style'], $c['size']);
					} else {
						$this->SetFont($c['family']);
					}
				}
				// Fixe la couleur pour les contours de cellules.
				if (
					in_array($c['border'], [1, 'L', 'T', 'R', 'B'])
					and strlen($c['drawcolor']) > 0
				){
					// explode() retournera systématiquement un tableau
					// qu'il y ait trois données séparées par des virgules ou une
					$color = explode(',', $c['drawcolor']);
					// avec trois valeurs, nous avons une couleur RBG
					if (isset($color[1]) and isset($color[2]))
					{
						$this->SetDrawColor(
							intval($color[0]),
							intval($color[1]),
							intval($color[2])
						);
					} else 
					// avec une valeur, nous avons un niveau de gris
					{
						$this->SetDrawColor(intval($color[0]));
					}
				}
				// Indique si le fond de la cellule doit être coloré ou transparent
				if ($c['fillcolor'] and $c['fill'] == 1) 
				{
					$color = explode(',', $c['fillcolor']);
					if (isset($color[1]) and isset($color[2]))
					{
						$this->SetFillColor(
							intval($color[0]),
							intval($color[1]),
							intval($color[2])
						);
					} else 
					{
						$this->SetFillColor(intval($color[0]));
					}
				}
				// Fixe la couleur pour le texte.
				if (strlen($c['textcolor'])) 
				{
					$color = explode(',', $c['textcolor']);
					if (isset($color[1]) and isset($color[2]))
					{
						$this->SetTextColor(
							intval($color[0]),
							intval($color[1]),
							intval($color[2])
						);
					} else 
					{
						$this->SetTextColor(intval($color[0]));
					}
				}
				#si la largeur d'un bloc est définie, c'est que l'on veut la contrôler
				if ($c['w'] > 0 ){
					$largeur = $this->GetStringWidth($c['txt']);
					if($largeur > $c['w']) {
						$nb_caract = strlen($c['txt']);
						$taille_caract = $largeur / $nb_caract;
						$taille_a_supprimer = $largeur - $c['w'];
						$caract_a_supprimer = round($taille_a_supprimer / $taille_caract);
						$c['txt'] = substr($c['txt'],0,-abs($caract_a_supprimer+2));
					}
				}
				// Imprime une cellule (zone rectangulaire)
				// avec éventuellement des bords,
				// un fond et une chaîne de caractères. 
				$this->Cell(
					$c['w'],      // Largeur de la cellule.
					$c['h'],      // Hauteur de la cellule.
					$c['txt'],    // Chaîne à imprimer.
					$c['border'], // Indique si des bords doivent être tracés autour de la cellule.
					$c['ln'],     // Indique où se déplace la position courante après l'appel à la méthode.
					$c['align'],  // Permet de centrer ou d'aligner le texte.
					$c['fill'],   // Indique si le fond de la cellule doit être coloré 
					$c['link']    // URL ou identifiant retourné par AddLink().
				);
			}
		}

	}
	
/**
 * Calcul des pages demandées
 *
 * @param  string          $brut Pagination sous forme d'énumération : 3,5,8
 *                                          ou d'intervale : 3-8
 * @return boolean|array   $tableau des pages listées une à une ou False si échec.
**/
function calcul_designation_pages($brut){
	$tableau = explode(',', $brut);
	if (is_string($tableau))
		{
			$tableau = array($tableau);
		} 
	$espace = array();
	foreach ($tableau as $cle => $valeur) 
	{
		if (!is_numeric($valeur)) 
		{
			// si ce n'est pas une énumération, cela ne peut être qu'un interval
			if (preg_match ('/([0-9]*)-([0-9]*)/', $valeur , $matches))
			{
				echo "matches = <pre>" . print_r($matches,true) . "</pre>";
				if ($matches[1] and $matches[1] < $matches[2])
				{
					$tableau[$cle] = $matches[1];
					for ($i = $matches[1]+1; $i <= $matches[2]; $i++)
					{
						$cle++;
						$tableau[$cle] = $i;
					}

				} else 
				{
					return false;
				}
			} else
			{
				return false;
			}
		}
	}
	
	return $tableau;
}