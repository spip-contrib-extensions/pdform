<?php

/**
  * Gestion de l'action `pdform_voir`
  *
  * Permet d'accéder, en téléchargement, au PDF
  *
  * @plugin Pdform pour Spip 4
  * @license GPL 2020-2022
  * @author Thrax
  *
  * @package SPIP\Pdform\Action
 **/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
  * Action qui propose de voir le fichier PDF
  * dont le chemin est transmis au travers d'une action sécurisée. 
  *
  * @param void
  *     Utilise l'argument de l'action sécurisée.
  * @return PDF
  *
 **/

function action_pdform_voir() {

	// Securisation de l'argument nom de l'archive
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$chemin_fichier = $securiser_action();
	include_spip('inc/autoriser');
	if (!autoriser('cache')) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// Si l'archive n'est pas accessible, on l'explique
	if (!@is_readable($chemin_fichier)) {
		include_spip('inc/minipres');
		echo minipres(_T('pdform:titre_erreur_cache'), _T('pdform:explication_erreur_cache'));
		exit;
	} else {

		// Vider tous les tampons pour ne pas provoquer de Fatal memory exhausted
		$level = @ob_get_level();
		while ($level--) {
			@ob_end_clean();
		}

		// Affichage du fichier cache
		header('Content-type: application/pdf');
		header('Content-Disposition: inline; filename="' . basename($chemin_fichier) . '"');
		header('Cache-Control: no-cache, must-revalidate'); // HTTP/1.1
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date dans le passé
		readfile($chemin_fichier);
	}
}
