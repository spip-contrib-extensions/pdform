<?php
/**
 * Gestion de l'action pdform_peuplement_exemples
 *
 * @plugin     Pdform
 * @copyright  2021-2022
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Pdform\Action
 */

 
if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Action pour créer des exemples (peuplement des tables du plugin).
 * 
 * @param  string    $arg non utilisé.
 * @return void
**/
 
function action_pdform_peuplement_exemples_dist($arg=null){
	include_spip('inc/autoriser');
	if (is_null($arg)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	if (!autoriser('peuplement', 'pdform')) {
		return;
	}

	// On lance le peuplement
	$message = action_peuplement_exemples_go();

	// On signale l'opération sur la BDD dans le log
	spip_log(_T('pdform_peuplement:log_peuplement', array(
				'id' => $GLOBALS['visiteur_session']['id_auteur'],
				'nom' => $GLOBALS['visiteur_session']['nom'],
				'mes' => $message
			)
		),
		'pdform.' . _LOG_INFO_IMPORTANTE
	);

	return $erreur;
}

/**
 * Fonction privé de peuplement
 *
 * @use editer_objet
 * @link https://www.spip.net/fr_article5526.html
 *
 * @param  string    $arg
 * @return void
 */

function action_peuplement_exemples_go($arg=null){
	$message_log = array();

	include_spip('action/editer_objet');

	# les patrons

	$inserts = array(); $correspondances = array();
	list($cles, $valeurs) = donnees_spip_pdform_patrons();
	if (is_array($valeurs)) {
		foreach ($valeurs as $v) { // le jeu de données
			$i = array();
			foreach ($v as $k => $valeur) { // que l'on décline pour affecter les clés
				if ($cles[$k] == 'id_pdform_patron'){ // on prend la valeur de référence de l'id qui sera créé
					$correspondances[] = $valeur;
					$i[$cles[$k]] = '';
				} else {
					$i[$cles[$k]] = $valeur;
				}
			}
			$inserts[] = $i;
		}
		unset($valeurs,$cles,$i);
		foreach ($inserts as $key => $tableaux) {
			$id_pdform_patron = objet_inserer('pdform_patron', '', $tableaux); // on insert l'objet
			$retrouve_id_pdform_patron[$correspondances[$key]] = $id_pdform_patron; // en gardant mémoire de son id
			// On signalera l'opération sur la BDD dans le log
			$message_log[] = "action_peuplement_exemples_go. objet_inserer.\nobjet = id_pdform_patron\nid_objet=$id_pdform_patron";
		}
	}
	
	# les cellules
	
	$inserts = array(); $correspondances = array();
	list($cles, $valeurs) = donnees_spip_pdform_cellules();
	if (is_array($valeurs)) {
		foreach ($valeurs as $v) { // le jeu de données
			$i = array();
			foreach ($v as $k => $valeur) { // que l'on décline pour affecter les clés
				if ($cles[$k] == 'id_pdform_cellule'){
					$correspondances[] = $valeur;
					$i[$cles[$k]] = '';
				} else {
					$i[$cles[$k]] = $valeur;
				}
			}
			$inserts[] = $i;
		}
		unset($valeurs,$cles,$i);
		foreach ($inserts as $key => $tableaux) {
			$id_pdform_cellule = objet_inserer('pdform_cellule', '', $tableaux);
			$retrouve_id_pdform_cellule[$correspondances[$key]] = $id_pdform_cellule;
			$message_log[] = "action_peuplement_exemples_go. objet_inserer.\nobjet = id_pdform_cellule\nid_objet=$id_pdform_cellule";
		}
	}

	# les liaisons

	include_spip('action/editer_liens');

	$inserts = array(); $correspondances = array();
	list($cles, $valeurs) = donnees_spip_pdform_cellules_liens();
	if (is_array($valeurs)) {
		foreach ($valeurs as $v) { // le jeu de données
			$i = array();
			foreach ($v as $k => $valeur) { // que l'on décline pour affecter les clés
				if ($cles[$k] == 'id_objet') { // et l'on retrouve l'identifiant
					$i[$cles[$k]] = $retrouve_id_pdform_patron[$valeur];
				} elseif ($cles[$k] == 'id_pdform_cellule') {
					$i[$cles[$k]] = $retrouve_id_pdform_cellule[$valeur];
				} else {
					$i[$cles[$k]] = $valeur;
				}
			}
			$inserts[] = $i;
		}
		unset($valeurs,$cles,$i);
		foreach ($inserts as $key => $tableaux) {
			$nb_liens = objet_associer(
				array('pdform_cellule' => $tableaux['id_pdform_cellule']),
				array('pdform_patron' => $tableaux['id_objet']),
				array('page' => $tableaux['page']),
			);
		}
		$message_log[] = "action_peuplement_exemples_go. objet_associer.\nobjet = pdform_patron\nid_objet=".$tableaux['id_objet'];
	}

	# les images
	
	$inserts = array(); $correspondances = array();
	list($cles, $valeurs) = donnees_spip_pdform_images();
	if (is_array($valeurs)) {
		foreach ($valeurs as $v) { // le jeu de données
			$i = array();
			foreach ($v as $k => $valeur) { // que l'on décline pour affecter les clés
				if ($cles[$k] == 'id_pdform_image'){
					$correspondances[] = $valeur;
					$i[$cles[$k]] = '';
				} else {
					$i[$cles[$k]] = $valeur;
				}
			}
			$inserts[] = $i;
		}
		unset($valeurs,$cles,$i);
		foreach ($inserts as $key => $tableaux) {
			$id_pdform_image = objet_inserer('pdform_image', '', $tableaux);
			$retrouve_id_pdform_image[$correspondances[$key]] = $id_pdform_image;
			$message_log[] = "action_peuplement_exemples_go. objet_inserer \nobjet = id_pdform_image\nid_objet=$id_pdform_image";
		}
	}

	# les liaisons

	$inserts = array(); $correspondances = array();
	list($cles, $valeurs) = donnees_spip_pdform_images_liens();
	if (is_array($valeurs)) {
		foreach ($valeurs as $v) { // le jeu de données
			$i = array();
			foreach ($v as $k => $valeur) { // que l'on décline pour affecter les clés
				if ($cles[$k] == 'id_objet') { // et l'on retrouve l'identifiant
					$i[$cles[$k]] = $retrouve_id_pdform_patron[$valeur];
				} elseif ($cles[$k] == 'id_pdform_image') {
					$i[$cles[$k]] = $retrouve_id_pdform_image[$valeur];
				} else {
					$i[$cles[$k]] = $valeur;
				}
			}
			$inserts[] = $i;
		}
		unset($valeurs,$cles,$i);
		foreach ($inserts as $key => $tableaux) {
			$nb_liens = objet_associer(
				array('pdform_image' => $tableaux['id_pdform_image']),
				array('pdform_patron' => $tableaux['id_objet']),
				array('page' => $tableaux['page']),
			);
			# ajouter l'information rang_lien
			if (intval($tableaux['rang_lien'])) {
				sql_updateq('spip_pdform_images_liens', array('rang_lien' => intval($tableaux['rang_lien'])), array('id_pdform_image=' . $tableaux['id_pdform_image'],'id_objet=' . $tableaux['id_objet'], 'objet="pdform_patron"'));
			}
			$message_log[] = "action_peuplement_exemples_go. objet_associer id_pdform_image n°" . $tableaux['id_pdform_image'] . "\nobjet = pdform_patron\nid_objet=".$tableaux['id_objet']."\nrang_lien = " . $tableaux['rang_lien'] . "\npage = ".$tableaux['page'];
		}
	}

	return $message_log;
}

/**
 * Donnees de la table spip_pdform_patrons
 *
 */
function donnees_spip_pdform_patrons() {

	$cles = array('id_pdform_patron', 'titre', 'pages', 'orientation', 'format', 'police', 'style', 'taille', 'couleur_trace', 'couleur_rempli', 'couleur_texte',
		'entete_police','entete_style', 'entete_taille', 'entete_couleur_trace', 'entete_couleur_rempli', 'entete_couleur_texte', 'entete_x', 'entete_y', 'entete_largeur', 'entete_hauteur', 'entete_texte', 'entete_bordure', 'entete_apres', 'entete_align', 'entete_plein', 'entete_lien', 'entete_pages',
		'pied_police','pied_style', 'pied_taille', 'pied_couleur_trace', 'pied_couleur_rempli', 'pied_couleur_texte', 'pied_x', 'pied_y', 'pied_largeur', 'pied_hauteur', 'pied_texte', 'pied_bordure', 'pied_apres', 'pied_align', 'pied_plein', 'pied_lien', 'pied_pages',
		'date', 'statut', 'maj');
	$valeurs = array(
		array(1, 'Exemples d\'utilisations', 3, 'P', 'A4', 'helvetica', '', '10', '0,0,0', '0,0,0', '0,0,0',
			'helvetica', '', '', '', '', '', '11', '10', '188', '0', '', '0', '10',  'C', '0', '', '2',
			'helvetica', 'I', '8', '0,0,0', '0,0,0', '0,0,0', '11', '-25', '188', '0', 'Page', '0', '',  'C', '0', '', '1,2',
			'2021-05-11 19:37:43', 'publie', '2021-05-11 19:37:43'),
	);
	return array($cles, $valeurs);
}


/**
 * Donnees de la table spip_pdform_cellules
 *
 * Exemple : 
 * La définition de la fenêtre pour une enveloppe fenêtre est :
 * un cadre de 3,3 cm de haut sur 8,5 cm de large. Celui-ci se situe respectivement à 5,5 cm du bord supérieur
 * de votre feuille et à 11 cm du bord gauche.
 */
function donnees_spip_pdform_cellules() {

	$cles = array('id_pdform_cellule', 'titre', 'table_nourriciere', 'police', 'style', 'taille', 'couleur_trace', 'couleur_rempli', 'couleur_texte', 'x', 'y', 'largeur', 'hauteur', 'texte', 'bordure', 'apres', 'align', 'plein', 'lien', 'maj'
	);
	$valeurs = array(
		array(1, 'texte site spip', '', 'helvetica', 'B', '11', '0,0,0', '0,0,0', '175,29,97', '15', '45.00', '0.00', '0.00', 'www.spip.net', '0', '2', 'L', '0', 'https://www.spip.net', '2021-05-22 13:40:54'),
		array(2, 'telephone spip', '', 'helvetica', 'B', '11', '0,0,0', '0,0,0', '175,29,97', '15', '50.00', '0.00', '0.00', '01 00 00 00 00', '0', '0', 'L', '0', 'tel:+33100000000', '2021-05-22 13:44:15'),
		// si le plugin Contacts est installé, il y a des nouveaux champs pour la table auteurs, et notamment la civilité, et le prénom de celui-ci. Si ce n'est pas le cas, sera placer une information de substitution transmise dans le flot (arguments lors de l'appel de la fonction de création du PDF)
		array(3, 'bloc prenom nom', 'spip_auteurs', 'helvetica', 'B', '11', '0,0,0', '0,0,0', '0,0,0', '110.00', '60.00', '85.00', '0.00', '@civilite@>@civilite_substitution@ @prenom@>@prenom_substitution@ @nom@>@nom_substitution@', '0', '2', 'L', '0', '', '2021-05-13 12:21:19'),
		// Transmission dans le flot d'une adresse fictive. Si le plugin Coordonnées est installé le spipeur pourait appeler une adresse enregistrée dans la base de données)
		/*
		array(4, 'bloc adresse', 'spip_adresses', 'helvetica', '', '11', '0,0,0', '0,0,0', '0,0,0', '110.00', '58.00', '85.00', '5.00', "@voie@\n|@complement@\n@code_postal@ @ville@", '0', '2', 'L', '0', '', '2021-05-22 14:42:41'),
		*/
		array(4, 'champs adresse', '', 'helvetica', '', '11', '0,0,0', '0,0,0', '0,0,0', '110.00', '65.00', '85.00', '5.00', "@voie_substitution@\n|@complement_substitution@\n@code_postal_substitution@ @ville_substitution@", '0', '2', 'L', '0', '', '2021-05-22 14:42:41'),
		array(5, 'argument date lettre', '', 'helvetica', '', '11', '', '', '', '110.00', '90.00', '0.00', '5.00', 'A Saint-Ouen, le @stamp_courrier@|affdate{\'d/m/Y\'}', '0', '0', 'L', '0', '', '2021-05-30 21:09:24'),
		array(6, 'texte president', '', 'helvetica', 'B', '11', '', '', '', '130.00', '223.00', '0.00', '5.00', "Alain Térieur du Pdf\nPrésident", '0', '2', 'L', '0', '', '2021-05-30 21:09:24'),
		array(7, 'champ lettre corps', 'spip_auteurs', 'helvetica', '', '11', '', '', '', '10.00', '120.00', '189.00', '5.00', "@prenom@>@bonjour@,\r\n\r\nVoici ta carte d'abonnement aux SPIP Party !", '0', '2', 'L', '0', '', '2021-05-30 21:17:07'),
		array(8, 'texte Formule de fin', '', 'helvetica', '', '11', '', '', '', '10.00', '200.00', '189.00', '5.00', "Spipeusement,", '0', '2', 'L', '0', '', '2021-05-30 21:17:07'),
		array(9, 'champs nom sur carte fonce', 'spip_auteurs', 'helvetica', '', '16', '', '', '175,29,97', '20.20', '145.20', '85.00', '5.00', "@prenom@>@prenom_substitution@ @nom@", '0', '2', 'C', '0', '', '2021-05-30 21:17:07'),
		array(10, 'champs nom sur carte blanc', 'spip_auteurs', 'helvetica', '', '16', '', '', '255,255,255', '20.00', '145.00', '85.00', '5.00', "@prenom@>@prenom_substitution@ @nom@", '0', '2', 'C', '0', '', '2021-05-30 21:17:07'),
		array(11, 'texte ordre', '', 'helvetica', '', '16', '', '', '175,29,97', '10', '10.00', '190', '5.00', "Ordre d'affichage", '0', '2', 'C', '0', '', '2021-05-30 21:17:07'),
		array(12, 'objet', '', 'helvetica', 'B', '16', '', '', '200,0,0', '-11', '30.00', '', '5.00', "Objets", '0', '2', 'R', '0', '', '2021-05-30 21:17:07'),
		array(13, 'texte image', '', 'helvetica', '', '16', '', '', '', '-11', '50.00', '', '5.00', "pdform_images", '0', '2', 'R', '0', '', '2021-05-30 21:17:07'),
		array(14, 'texte cellule', '', 'helvetica', '', '16', '', '', '', '-11', '110.00', '', '5.00', "pdform_cellules", '0', '2', 'R', '0', '', '2021-05-30 21:17:07'),
		array(15, 'flot', '', 'helvetica', 'B', '16', '', '', '200,0,0', '20', '180.00', '', '5.00', "Flots", '0', '2', 'L', '0', '', '2021-05-30 21:17:07'),
		array(16, 'texte formes', '', 'helvetica', '', '16', '', '', '', '20', '200.00', '', '5.00', "formes", '0', '2', 'L', '0', '', '2021-05-30 21:17:07'),
		array(17, 'texte textes', '', 'helvetica', '', '16', '', '', '', '20', '240.00', '', '5.00', "textes", '0', '2', 'L', '0', '', '2021-05-30 21:17:07'),
	);
	return array($cles, $valeurs);
}


/**
 * Donnees de la table spip_pdform_cellules_liens
 *
 */
function donnees_spip_pdform_cellules_liens() {

	$cles = array('id_pdform_cellule', 'id_objet', 'objet', 'page', 'vu');
	$valeurs = array(
		// transmission d'une carte
		array(1, 1, 'pdform_patron', 1, 'non'),  // Pour le logo : - https://www.spip.net
		array(2, 1, 'pdform_patron', 1, 'non'),  //                - téléphone de SPIP (faux)
		array(3, 1, 'pdform_patron', 1, 'non'),  // nom de l'auteur à qui adresser la carte 
		array(4, 1, 'pdform_patron', 1, 'non'),  // adresse de l'auteur
		array(5, 1, 'pdform_patron', 1, 'non'),  // A Saint-Ouen le xxx
		array(6, 1, 'pdform_patron', 1, 'non'),  // Pour la signature : titre président
		array(7, 1, 'pdform_patron', 1, 'non'),  // Corps de la lettre
		array(8, 1, 'pdform_patron', 1, 'non'),  // fin de lettre, formule de politesse
		array(9, 1, 'pdform_patron', 1, 'non'),  // effet
		array(10, 1, 'pdform_patron', 1, 'non'),  // prénom et nom sur la carte si champs extra, sinon nom
		array(11, 1, 'pdform_patron', 2, 'non'),  // titre
		array(12, 1, 'pdform_patron', 2, 'non'),  // objet
		array(13, 1, 'pdform_patron', 2, 'non'),  // images
		array(14, 1, 'pdform_patron', 2, 'non'),  // cellules
		array(15, 1, 'pdform_patron', 2, 'non'),  // flots
		array(16, 1, 'pdform_patron', 2, 'non'),  // formes
		array(17, 1, 'pdform_patron', 2, 'non'),  // textes
		
	);
	return array($cles, $valeurs);
}

/**
 * Donnees de la table spip_pdform_images
 *
 */
function donnees_spip_pdform_images() {

	$cles = array('id_pdform_image', 'titre', 'imgfile', 'imgx', 'imgy', 'imgw', 'imgh', 'imgtype', 'imglink', 'maj'); 
	$valeurs = array(
		array(1, 'logo spip lettre', 'exemples/accessoires/spip_logo_231x172.png', '11.00', '11.00', '23.10', '17.20', '', '', '2021-05-21 19:46:02'),
		// il vous est possible de mettre le chemin que vous souhaitez pour vos images (et notamment de les mettre dans le répertoire IMG/png)
		array(2, 'signature president', 'exemples/signatures/alain_terieur_873x242.png', '130.00', '210.00', '43.65', '10.50', '', '', '2021-05-16 14:04:48'),
		// le format 85 mm x 55 mm. Ce format correspond à celui d'une carte bancaire, et peut donc se glisser facilement dans n'importe quel portefeuille ou porte-cartes
		array(3, 'accessoire carte', 'exemples/accessoires/carte_vierge_850x550.png', '20', '140', '85', '', '', '', '2021-05-15 09:57:29'),
		array(4, 'logo spip carte', 'exemples/accessoires/spip_pastille_256x256.png', '25.00', '165.00', '25', '25', '', '', '2021-05-21 19:46:02'),
		// le format 210 mm x 297 mm est celui d'une feuille A4
		array(5, 'ordre affichages', 'exemples/accessoires/ordre_affichages_2100x2970.png', '0.00', '0.00', '210.00', '297.00', '', '', '2021-05-16 14:04:48'),
	);
	return array($cles, $valeurs);
}

/**
 * Donnees de la table spip_pdform_images_liens
 *
 */
function donnees_spip_pdform_images_liens() {

	$cles = array('id_pdform_image', 'id_objet', 'objet', 'page', 'rang_lien');
	$valeurs = array(
		// TRESORERIE transmission carte
		array(1, 1, 'pdform_patron', 1, 0), // logo de SPIP
		array(2, 1, 'pdform_patron', 1, 1), // signatur du Président
		array(3, 1, 'pdform_patron', 1, 1), // carte blanche
		array(4, 1, 'pdform_patron', 1, 1), // carte blanche
		array(5, 1, 'pdform_patron', 2, 1), // ordre affichages
		
	);
	return array($cles, $valeurs);
}

