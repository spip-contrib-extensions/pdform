<?php


/**
 * Gestion de l'action page_haut
 *
 * @plugin     Pdform
 * @copyright  2021-2022
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Pdform\Action
 **/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action 
 *
 * L'argument attendu est `pdform_image-#ID_PDFORM_IMAGE-#OBJET-#ID_OBJET-#PAGE-haut`
 * tel que `pdform_image-4-pdform_patron-1-0-haut`.
 *
 * @uses table_objet()
 * @uses id_table_objet()
 *
 * @param null|string $arg
 *     Clé des arguments. En absence utilise l'argument
 *     de l'action sécurisée.
 * @return void
 */
function action_pdform_page_dist($arg = null) {
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();

	$arg = explode("-", $arg);
	list($type, $id_objet_source, $objet, $id_objet, $page, $direction) = $arg;

	include_spip('inc/filtres'); // fonctions pour les objets (id_table_objet et lister_tables_objets_sql)
	
	$table_objet = table_objet($type);
	$id_table_objet = id_table_objet($table_objet);

	spip_log("action_pdform_page_dist. arg. " . print_r($arg, true) . "table_objet=$table_objet id_table_objet=$id_table_objet", 'pdform.' . _LOG_DEBUG);

	switch ($direction) {
		case 'haut':
			sql_updateq("spip_{$table_objet}_liens", array('page' => $page + 1), array("objet='{$objet}'", 'id_objet=' . intval($id_objet), $id_table_objet .'=' . intval($id_objet_source)));
			break;
		case 'bas':
			if ($page > 1) {
			sql_updateq("spip_{$table_objet}_liens", array('page' => $page - 1), array("objet='{$objet}'", 'id_objet=' . intval($id_objet), $id_table_objet .'=' . intval($id_objet_source)));
			}
			break;
	}
}
