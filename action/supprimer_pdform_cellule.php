<?php
/**
 * Utilisation de l'action supprimer pour l'objet ’pdform_cellule’
 *
 * @plugin     Pdform
 * @copyright  2021
 * @author     Thrax
 * @licence    GNU/GPL
 * @package    SPIP\Pdform\Action
 */
 
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}



/**
 * Action pour supprimer une cellule
 *
 * Vérifier l'autorisation avant d'appeler l'action.
 *
 * @param null|int $arg
 *     Identifiant à supprimer.
 *     En absence de id utilise l'argument de l'action sécurisée.
**/
function action_supprimer_pdform_cellule_dist($arg=null) {
	if (is_null($arg)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}
	$arg = intval($arg);

	// cas suppression
	if ($arg) {
		sql_delete('spip_pdform_cellules',  'id_pdform_cellule=' . sql_quote($arg));
	}
	else {
		spip_log("action_supprimer_pdform_cellule_dist $arg pas compris");
	}
}
