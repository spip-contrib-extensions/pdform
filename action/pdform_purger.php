<?php

/**
  * Gestion de l'action `pdform_purger`
  *
  * Permet de supprimer le cache d'un PDF
  *
  * @plugin Pdform pour Spip 4
  * @license GPL 2020-2022
  * @author Thrax
  *
  * @package SPIP\Pdform\Action
 **/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action qui propose de supprimer le fichier PDF
 * dont le chemin est transmis au travers d'une action sécurisée. 
 *
 * demander #AUTORISER{purger,pdform} avant de proposer cette action.
 *
 * @param void
 *     Utilise l'argument de l'action sécurisée.
 * @return void
 *
 */

function action_pdform_purger_dist() {

	// Securisation de l'argument nom de l'archive
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$chemin_fichier = $securiser_action();

	// Si l'archive n'est pas accessible on renvoie une erreur
	if (!@is_readable($chemin_fichier)) {
		spip_log("Le fichier {$chemin_fichier} n'est pas accessible pour être purgé", 'pdform.' . _LOG_ERREUR);
	} else {

	// Suppression (pruge) du cache
		unlink($chemin_fichier);
		spip_log("Le fichier {$chemin_fichier} a été supprimé", 'pdform.' . _LOG_DEBUG);
	}
}