<?php

if (!defined('_ECRIRE_INC_VERSION')){
	return;
}
# les constantes nécessaires
if (!defined('_DIR_LIB')) define('_DIR_LIB', _DIR_RACINE . 'lib/'); // la constante _DIR_LIB est définie /inc/utils.php
if (!defined('_DIR_FPDF_LIB')) define('_DIR_FPDF_LIB', _DIR_LIB . 'fpdf185/'); // definition du repertoire de la librairie FPDF
if (!defined('FPDF_FONTPATH')) define('FPDF_FONTPATH', _DIR_FPDF_LIB .'font/'); // les polices utilisables par le PDF
if (!defined('_FPDF_FORMATS_IMG')) define('_FPDF_FORMATS_IMG', ['jpg' => 'jpg', 'jpeg' => 'jpeg', 'png' => 'png', 'gif' => 'gif']); // les formats d'images acceptés
